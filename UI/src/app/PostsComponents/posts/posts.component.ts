import { Component, OnInit } from '@angular/core';
import {Post} from "../../model/Post";
import {PostService} from "../../services/post.service";
import {User} from "../../model/User";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  public posts: Post[];
  private loggedUser :User;
  public  page: number =1;

  constructor(private  postService : PostService ) { }

  ngOnInit(): void {
    this.getLoggedUser();
    this.getPosts();
  }
  getLoggedUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    this.loggedUser  = JSON.parse(loggedUserFromSession);
  }
  getPosts() {
    let pageInString = this.page.toString();
    this.postService.getPostsForTeam(this.loggedUser.team.id, pageInString)
      .subscribe(postsFromBase=>this.posts = postsFromBase)
  }

  setPage(number: number) {
    this.page= this.page+number;
    this.getPosts()


  }
}


