import { Component, OnInit } from '@angular/core';
import {Post} from "../../model/Post";
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../../services/post.service";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  public  post:Post

  constructor(
    private route: ActivatedRoute,
    private postService:PostService
  ) { }

  ngOnInit()  : void {
    this.getPost()
  }

  getPost() {
    const id = +this.route.snapshot.paramMap.get('id')
    this.postService.getPostById(id)
      .subscribe(postFromService => this.post = postFromService);


  }
}
