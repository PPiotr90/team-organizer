import { Component, OnInit } from '@angular/core';
import {PostService} from "../../services/post.service";
import {Post} from "../../model/Post";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {


  public loggedUser

  constructor( private  postService : PostService) { }

  ngOnInit(): void {
    this.getLoggedUser()

  }

  getLoggedUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    this.loggedUser = JSON.parse(loggedUserFromSession)

  }

  createPost(topic: string, content: string) {
    let post = {topic, content}
    let userOptions = {
      'authorLogin': this.loggedUser.username,
      'teamId' : this.loggedUser.team.id
    }
    post=Object.assign(post, userOptions)
    let postInJson = JSON.stringify(post);
    this.postService.createPost(postInJson)
      .subscribe(response => window.close())

  }
}
