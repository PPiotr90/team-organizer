import { Component, OnInit } from '@angular/core';
import {Rate} from "../model/Rate";
import {ExchangeService} from "../services/exchange.service";

@Component({
  selector: 'app-exchanges',
  templateUrl: './exchanges.component.html',
  styleUrls: ['./exchanges.component.css']
})


export class ExchangesComponent implements OnInit {
public  rates: Rate[]
  constructor(private  exchangeService: ExchangeService) { }

  ngOnInit(): void {

  this.getRates()
  }

  getRates() {
  this.exchangeService.getExchanges()
    .subscribe(rates=> this.rates = rates)
  }

}
