import { Component, OnInit } from '@angular/core';
import {ProjectService} from "../services/project.service";
import {Project} from "../model/Project";
import {User} from "../model/User";
import {Role} from "../model/Role";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  public role: String;
  public projects: Project[];
  public showCreateProject: boolean = false

  constructor(private projectService: ProjectService) {
  }

  ngOnInit(): void {
    this.getProjects()
    this.setRole()
  }

  getProjects() {
    this.projectService
      .getProjects()
      .subscribe(projects => this.projects = projects);
  }


  changeShowCreator() {
    this.showCreateProject = !this.showCreateProject
  }
  setRole() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    console.log(loggedUserFromSession)
    let loggedUser :User = JSON.parse(loggedUserFromSession);
    this.role = loggedUser.role;

  }
}
