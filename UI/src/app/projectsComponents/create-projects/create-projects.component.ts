import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProjectService} from "../../services/project.service";
import {UserServiceService} from "../../services/user-service.service";
import {ProjectPrefix} from "../../model/Project";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-projects',
  templateUrl: './create-projects.component.html',
  styleUrls: ['./create-projects.component.css']
})
export class CreateProjectsComponent implements OnInit {
public  usernames: String[];
public  prefixes :String[] = [];
public created: boolean = false
  @Output() createProjectEmmiter = new EventEmitter;

  constructor(private projectService: ProjectService,
              private  userService: UserServiceService,
              private router: Router) { }

  ngOnInit(): void {
    this.setUsernames()
    this.setPrefixes()
  }

  setUsernames() {
    this.userService.getUsernames()
      .subscribe(usernames =>this.usernames = usernames)
  }

  setPrefixes() {
    Object.keys(ProjectPrefix).map(key => ProjectPrefix[key])
      .filter(value => isNaN(Number(value)) === true)
      .forEach(prefix => this.prefixes.push(prefix));
    console.log(this.prefixes)
  }

  createProject(prefix, leader, topic, finishDate, description) {
  let newProject = {'projectPrefix': prefix, 'leader': leader, 'topic': topic, 'finishDate': finishDate
  ,'description': description}
  let projectToCreate = JSON.stringify(newProject);
    console.log(projectToCreate);
    this.projectService.save(projectToCreate).subscribe(response => {
      this.createProjectEmmiter.emit();
      this.created = true;
      });

  }
}
