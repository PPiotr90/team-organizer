import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponentComponent} from "./userComponents/login-component/login-component.component";
import {RegistrationComponent} from "./userComponents/registration/registration.component";
import {MainPageComponent} from "./main-page/main-page.component";

import {NotLoggedUserComponent} from "./not-logged-user/not-logged-user.component";


import {RouterModule, Routes} from "@angular/router";


const routes: Routes = [

  {path: '', component: MainPageComponent},

  {
    path: 'registration',
    component: RegistrationComponent},
  {
    path: "login",
    component: LoginComponentComponent},
  {
    path: "unauthorized",
    component: NotLoggedUserComponent},
  {
    path: 'userPage',
    data: {preload: true},
    loadChildren: 'src/app/userComponents/user.module#UserModule'},
  {path: "**", redirectTo: "unauthorized"},
]

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {
}
