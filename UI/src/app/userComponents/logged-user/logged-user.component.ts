import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../model/User";
import {UserServiceService} from "../../services/user-service.service";

@Component({
  selector: 'app-logged-user',
  templateUrl: './logged-user.component.html',
  styleUrls: ['./logged-user.component.css']
})
export class LoggedUserComponent implements OnInit {
  public page
  public role: String
  public  loggedUser: User;

  constructor(private route: Router,
              private  userService: UserServiceService) {
  }

  ngOnInit(): void {
    this.getLoggedUser();

  }

  getLoggedUser() {
   let loggedUserFromSession: User =  JSON.parse(sessionStorage.getItem("loggedUser"))
    this.userService.getUserByUsername(loggedUserFromSession.username)
      .subscribe(user => this.loggedUser = user)
    this.setRole()
  }

  logout() {
    sessionStorage.removeItem('loggedUser')
    this.route.navigate(['/'])
  }

  changePage(value) {
    this.getLoggedUser();
    this.route.navigate(["userPage/"+value])
  }
  setRole() {
    this.role = this.loggedUser.role;
  }


}

