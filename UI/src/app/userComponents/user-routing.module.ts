import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoggedUserComponent} from "./logged-user/logged-user.component";
import  {UserPageComponent} from "./user-page/user-page.component";
import {ProjectsComponent} from "../projectsComponents/projects.component";
import {RegistryComponent} from "../registry/registry.component";
import {AbsencesComponent} from "../absences/absences.component";
import {NewNoteComponent} from "./noteComponents/new-note/new-note.component";
import {NewLinkComponent} from "./linkComponents/new-link/new-link.component";
import {NewPostComponent} from "../PostsComponents/new-post/new-post.component";
import {PostComponent} from "../PostsComponents/post/post.component";


const routes: Routes = [
  {
    path: "", component: LoggedUserComponent,
    children: [
      {path: '', component: UserPageComponent},

      {
        path: "projects",
        component: ProjectsComponent},
      {
        path: "registry",
        component: RegistryComponent},
      {
        path: "absences",
        component: AbsencesComponent},
      {
        path: "newNote",
        component: NewNoteComponent},
      {
        path: "newLink",
        component: NewLinkComponent},
      {path : "newPost",
      component: NewPostComponent},
      {
        path: "post/:id",
        component: PostComponent},
      {
        path: "messages",
        data: {preload: true},
        loadChildren: 'src/app/messagesComponents/messages.module#MessagesModule'}
    ]
  }
  ];
@NgModule({
imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
