
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import  {UserRoutingModule} from "./user-routing.module";

 import  {UserPageComponent} from "./user-page/user-page.component";
 import {LoggedUserComponent} from "./logged-user/logged-user.component";
import  {NewNoteComponent} from "./noteComponents/new-note/new-note.component";
import {NotesComponent} from "./noteComponents/notes/notes.component";
import  {NewLinkComponent} from "./linkComponents/new-link/new-link.component";
import  {LinksComponent} from "./linkComponents/links/links.component";
import {ProjectsComponent} from "../projectsComponents/projects.component";
import {RegistryComponent} from "../registry/registry.component";
import {AbsencesComponent} from "../absences/absences.component";
import {MessagesModule} from "../messagesComponents/messages.module";
import {CreateProjectsComponent} from "../projectsComponents/create-projects/create-projects.component";
import {PostsComponent} from "../PostsComponents/posts/posts.component";
import  {PostViewComponent} from "../PostsComponents/post-view/post-view.component";
import  {WeatherComponent} from  '../weather/weather.component'
import { ExchangesComponent } from '../exchanges/exchanges.component';



@NgModule({
  declarations: [
    UserPageComponent,
    LoggedUserComponent,
    NewNoteComponent,
    NotesComponent,
    NewLinkComponent,
    LinksComponent,
    ProjectsComponent,
    RegistryComponent,
    AbsencesComponent,
    CreateProjectsComponent,
    PostsComponent,
    PostViewComponent,
    WeatherComponent,
    ExchangesComponent,
  ],
    imports: [
        CommonModule,
        UserRoutingModule,
        MessagesModule,


    ],
  providers: [],
  exports: [
    WeatherComponent,
    ExchangesComponent
  ],
  bootstrap: [LoggedUserComponent]
})
export class UserModule {}
