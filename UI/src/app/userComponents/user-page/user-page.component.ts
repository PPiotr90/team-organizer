import { Component, OnInit } from '@angular/core';
import {User} from "../../model/User";
import {UserServiceService} from "../../services/user-service.service";

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  public role: String
  public  loggedUser: User;

  constructor(private userService: UserServiceService) {
  }

  ngOnInit(): void {
    this.getLoggedUser()
  }

  getLoggedUser() {
    let loggedUserFromSession: User =  JSON.parse(sessionStorage.getItem("loggedUser"))
    this.userService.getUserByUsername(loggedUserFromSession.username)
      .subscribe(user => this.loggedUser = user)
    this.setRole()
  }

  newWindow(componentName: string) {
    window.open("/userPage/"+componentName, "CNN_WindowName",
      "menubar=no,location=no,resizable=no,scrollbars=no,status=no, height=300px," +
      "width=500px, left = 150px, top=150px")
  }
  setRole() {
    this.role = this.loggedUser.role;
  }
}
