import { Component, OnInit } from '@angular/core';
import {UserServiceService} from "../../../services/user-service.service";
import {User} from "../../../model/User";

@Component({
  selector: 'app-new-link',
  templateUrl: './new-link.component.html',
  styleUrls: ['./new-link.component.css']
})
export class NewLinkComponent implements OnInit {
  public loggedUser;
  constructor(private userService: UserServiceService) { }

  ngOnInit(): void {
this.getLoggedUser()

  }

  getLoggedUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    this.loggedUser = JSON.parse(loggedUserFromSession)
  }
  createLink(name, content) {
    let link  = {name, content}
    let linkInJson = JSON.stringify(link);
    this.userService.saveToUserArray("Link", this.loggedUser.username,linkInJson )
      .subscribe(response => window.close());



  }

}
