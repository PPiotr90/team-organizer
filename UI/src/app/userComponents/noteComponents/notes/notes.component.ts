import {Component, Input, OnInit} from '@angular/core';
import {Note} from "../../../model/Note";

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  @Input() notes: Note[]

  constructor() {
  }

  ngOnInit(): void {
  }


  showNote(note: Note) {
    note.show = !note.show

  }

  deleteNote(id: number) {

  }
}
