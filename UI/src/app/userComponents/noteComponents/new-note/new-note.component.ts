import { Component, OnInit } from '@angular/core';
import {UserServiceService} from "../../../services/user-service.service";

@Component({
  selector: 'app-new-note',
  templateUrl: './new-note.component.html',
  styleUrls: ['./new-note.component.css']
})
export class NewNoteComponent implements OnInit {
  public loggedUser
  constructor(private  userService : UserServiceService) { }

  ngOnInit(): void {
    this.getLoggedUser()

  }

  getLoggedUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    this.loggedUser = JSON.parse(loggedUserFromSession)
  }


  createNote(topic : string, content : string) {
    let note = {topic, content}
    let noteInJson = JSON.stringify(note)
    this.userService.saveToUserArray("Note",this.loggedUser.username, noteInJson)
      .subscribe(response => window.close());
  }

}
