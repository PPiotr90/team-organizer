import {Component, OnInit} from '@angular/core';

import {UserServiceService} from "../../services/user-service.service";
import { Router} from "@angular/router";



@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
public usernames : String[];
  constructor(private userService: UserServiceService,
              private route: Router) { }

  ngOnInit(): void {
this.setUsernames()
  }
  setUsernames() {
    this.userService.getUsernames()
      .subscribe(usernames =>this.usernames = usernames)
  }

  login(loggedUserName:String  ) {
    //TODO set login as two inputs and check password in backend


    this.userService.getUserByUsername(loggedUserName)
      .subscribe(user=> {
        console.log(user);
        console.log(user.team);
        sessionStorage.setItem("loggedUser", JSON.stringify(user))

    let navigationDetails: String[] =['/userPage']
    this.route.navigate(navigationDetails)
  })
  }
}
