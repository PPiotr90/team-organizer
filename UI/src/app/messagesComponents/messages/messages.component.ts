import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MessageServiceService} from "../../services/message-service.service";
import {User} from "../../model/User";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  unreadMessages: number;



  constructor(private route: Router,
              private  messageService: MessageServiceService) {
  }

  ngOnInit(): void {

    this.setUnreadMessage();
  }

  setContent(content: String) {
this.route.navigate(['/userPage/messages/'+content])
  }

  setUnreadMessage() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    console.log(loggedUserFromSession)
    let loggedUser :User = JSON.parse(loggedUserFromSession);
    let loggedUsername = loggedUser.username
    this.messageService.getNumberOfUnreadMessage(loggedUsername).subscribe(number => {
      let inboxRow = document.getElementById('inBoxRow');
      if (number != 0) {
        inboxRow.innerHTML = '<b>Inbox(' + number + ')</b>';
        console.log('inbox')
      } else {
        inboxRow.innerHTML = 'Inbox';
      }
    })
  }


}
