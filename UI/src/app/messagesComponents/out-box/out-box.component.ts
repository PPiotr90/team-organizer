import { Component, OnInit } from '@angular/core';
import {Message} from "../../model/Message";
import {MessageServiceService} from "../../services/message-service.service";
import {User} from "../../model/User";

@Component({
  selector: 'app-out-box',
  templateUrl: './out-box.component.html',
  styleUrls: ['./out-box.component.css']
})
export class OutBoxComponent implements OnInit {
  messages: Message[];
  showMessage: Message;

  constructor( private  messageService: MessageServiceService) { }

  ngOnInit(): void {
    this.getMessages()
    this.showMessage =undefined;
  }

  getMessages() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    let loggedUser :User = JSON.parse(loggedUserFromSession);
    let loggedUsername = loggedUser.username
    this.messageService.getMessagesForSender(loggedUsername)
      .subscribe(messages => this.messages = messages)
  }

  show(messageToShow: Message) {
    this.showMessage = messageToShow;
  }

  deleteChosen() {
    let ids: number[] = [];
    (document.getElementsByName("checkMessage") as unknown as HTMLInputElement[]).forEach(box=> {

      if(box.checked) ids.push(Number.parseInt(box.value))
    });
console.log(ids)
    this.messageService.deleteAsSender(ids).subscribe(response => this.getMessages())
    ids = [];
    this.showMessage = null;
  }
}
