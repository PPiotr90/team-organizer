import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MessageServiceService} from "../../services/message-service.service";
import {Message} from "../../model/Message";
import {User} from "../../model/User";


@Component({
  selector: 'app-in-box',
  templateUrl: './in-box.component.html',
  styleUrls: ['./in-box.component.css']

})
export class InBoxComponent implements OnInit {
  public messages: Message[];
  showMessage: Message;
  @Output() readMessage = new EventEmitter();



  constructor(private messageService: MessageServiceService) {
  }
  ngOnInit(): void {
    this.getMessages()
  }

  getMessages() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
     let loggedUser :User = JSON.parse(loggedUserFromSession);
     let loggedUsername = loggedUser.username
    this.messageService.getMessagesForReceiver(loggedUsername)
      .subscribe(messages => this.messages = messages)
  }

  deleteChosen() {
    let ids: number[] = [];

    (document.getElementsByName("checkMessage") as unknown as HTMLInputElement[]).forEach(box=> {
      if(box.checked) ids.push(Number.parseInt(box.value))
    });

      this.messageService.deleteAsReceiver(ids).subscribe(response => this.getMessages())
    ids = [];
      this.showMessage = null;
  }


  show(messageToShow: Message) {
    this.showMessage = messageToShow;
    if(!messageToShow.read)
    this.messageService.setRead(messageToShow.id).subscribe(response => this.readMessage.emit());

  }

}

