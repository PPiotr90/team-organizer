import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MessagesComponent} from "./messages/messages.component";
import  {InBoxComponent} from "./in-box/in-box.component";
import  {OutBoxComponent} from "./out-box/out-box.component";
import  {NewMessageComponent} from "./new-message/new-message.component";
import {MessagesRoutingModule} from "./messages-routing.module";
import {ShowMessageComponent} from "./show-message/show-message.component";



@NgModule({
  declarations: [
    MessagesComponent,
    InBoxComponent,
    OutBoxComponent,
    NewMessageComponent,
    ShowMessageComponent
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
  ],
  providers: [],
  exports: [],
  bootstrap: [MessagesComponent]
})
export class MessagesModule {}
