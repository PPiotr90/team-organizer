import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../../model/Message";

@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.css']
})
export class ShowMessageComponent implements OnInit {
  @Input() message: Message

  constructor() { }

  ngOnInit(): void {
  }

}
