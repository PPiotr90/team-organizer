import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {InBoxComponent} from "./in-box/in-box.component";
import {OutBoxComponent} from "./out-box/out-box.component";
import {NewMessageComponent} from "./new-message/new-message.component";
import {MessagesComponent} from "./messages/messages.component";

const routes: Routes = [
  {
    path: '',
    component: MessagesComponent,
    children: [

      {
        path: 'inbox',
        component: InBoxComponent
      },
      {
        path: 'outbox',
        component: OutBoxComponent
      },
      {
        path: 'newMessage',
        component: NewMessageComponent
      },
      {
        path: '**',
        redirectTo: 'inbox'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MessagesRoutingModule {
}
