import {Component, OnInit} from '@angular/core';
import {UserServiceService} from "../../services/user-service.service";
import {MessageServiceService} from "../../services/message-service.service";
import {User} from "../../model/User";

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit {
  public receivers: String[];
  private loggedUserUsername;
  public  created : boolean = false;

  constructor(private  userService: UserServiceService,
              private messageService: MessageServiceService) {
  }

  ngOnInit(): void {
    this.setLoggedUsername()
    this.setReceivers()
  }

  private setReceivers() {
    this.userService.getUsernames()
      .subscribe(usernames => {
        this.receivers =[]
          usernames.filter(username => username != this.loggedUserUsername)
            .forEach(username => this.receivers.push(username))

        })

  }

  private setLoggedUsername() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    let loggedUser: User = JSON.parse(loggedUserFromSession);
    this.loggedUserUsername = loggedUser.username
    console.log(this.loggedUserUsername)
  }

  send(receiver, topic, content) {

    let message = {'sender': this.loggedUserUsername, 'receiver': receiver, 'topic': topic, 'content': content}
    let messageToSend = JSON.stringify(message);
    this.messageService.sendMessage(messageToSend).subscribe();
    this.created = true;
  }
}
