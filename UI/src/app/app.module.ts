import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';
import {LoginComponentComponent} from './userComponents/login-component/login-component.component';
import {FormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {MainPageComponent} from './main-page/main-page.component';
import {RegistrationComponent} from './userComponents/registration/registration.component';

import {NotLoggedUserComponent} from './not-logged-user/not-logged-user.component';
import {UserModule} from "./userComponents/user.module";
import { NewPostComponent } from './PostsComponents/new-post/new-post.component';
import { PostComponent } from './PostsComponents/post/post.component';
import { CommentViewComponent } from './commentsComonents/comment-view/comment-view.component';
import { NewCommentComponent } from './commentsComonents/new-comment/new-comment.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    MainPageComponent,
    RegistrationComponent,
    NotLoggedUserComponent,
    NewPostComponent,
    PostComponent,
    CommentViewComponent,
    NewCommentComponent,



  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    UserModule
  ],
  providers: [],
    exports: [

    ],
  bootstrap: [AppComponent]
})
export class AppModule {}
