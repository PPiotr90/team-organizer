import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotLoggedUserComponent } from './not-logged-user.component';

describe('NotLoggedUserComponent', () => {
  let component: NotLoggedUserComponent;
  let fixture: ComponentFixture<NotLoggedUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotLoggedUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotLoggedUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
