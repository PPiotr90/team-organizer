import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-logged-user',
  templateUrl: './not-logged-user.component.html',
  styleUrls: ['./not-logged-user.component.css']
})
export class NotLoggedUserComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
