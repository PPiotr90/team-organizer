import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import DateTimeFormat = Intl.DateTimeFormat;
import {Message} from "../model/Message";

@Injectable({
  providedIn: 'root'
})


export class MessageServiceService {

  private url = "http://localhost:8080/messages";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }


  constructor(private httpClient: HttpClient) {
  }

  sendMessage(messageToSend: string) {
    return this.httpClient.post<Message>(this.url, messageToSend, this.httpOptions)
  }
    getNumberOfUnreadMessage(receiver: String) {
let url = this.url+'/unread/'+receiver
      return this.httpClient.get<number>(url)
    }

    getMessagesForSender(sender: String) {
    let url = this.url+'/sender/'+sender
      return this.httpClient.get<Message[]>(url)
    }
    getMessagesForReceiver(receiver: String) {
      let url = this.url+'/receiver/'+receiver
      return this.httpClient.get<Message[]>(url)
    }

  setRead(id: number) {
    let url = this.url+'/read/'+id
    return this.httpClient.patch(url, this.httpOptions)

  }

  deleteAsSender(ids: number[]) {
    let url = this.url+"/delete/sender"
    return this.httpClient.patch(url,ids, this.httpOptions)
  }
  deleteAsReceiver(ids: number[]) {
    let url = this.url+"/delete/receiver"
    return this.httpClient.patch(url, ids, this.httpOptions)
  }
}
