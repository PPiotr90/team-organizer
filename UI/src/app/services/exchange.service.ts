import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Rate} from "../model/Rate";

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  private url = "http://localhost:8080/exchanges";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }

  constructor(private httpClient: HttpClient) { }

  getExchanges() {
    return this.httpClient.get<Rate[]>(this.url, this.httpOptions)
  }
}
