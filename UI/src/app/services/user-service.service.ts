import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../model/User";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private   url = "http://localhost:8080/users";
  private httpOptions = {
    headers: new HttpHeaders({'Content-type': 'application/json'})
  }
  getUsernames() {
    let usernamesURL = this.url + "/usernames"
    return this.httpClient.get<String[]>(usernamesURL, this.httpOptions)
  }
  constructor( private  httpClient: HttpClient) { }
getUserByUsername( username: String) {
    let url = this.url+"/username/"+username
  return this.httpClient.get<User>(url, this.httpOptions)
}

  saveToUserArray(contentType: string, username: string, objectInJson: string) {

    let url = this.url+"/"+username+"/add"+contentType;
    return this.httpClient.post(url, objectInJson, this.httpOptions)

  }

  updateWeatherInUser(userToUpdate: User) {
    console.log(userToUpdate)
    return this.httpClient.put<User>(this.url, userToUpdate, this.httpOptions)
  }
}
