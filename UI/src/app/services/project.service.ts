import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Project } from '../model/Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private url = "http://localhost:8080/projects";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }
  constructor(private httpClient: HttpClient) { }

  public getProjects() {
    return this.httpClient.get<Project[]>(this.url);
  }

  public save(project: String ) {
    return this.httpClient.post<Project>(this.url,project, this.httpOptions )
  }

}
