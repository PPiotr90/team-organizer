import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private url = "http://localhost:8080/comments";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }

  constructor(private httpClient: HttpClient) { }


  createComment(commentInJson : string){
    return this.httpClient.post(this.url, commentInJson, this.httpOptions);
  }
}
