import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Post} from "../model/Post";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private url = "http://localhost:8080/posts";
  private httpOptions = {
    headers: new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8')
  }

  constructor(private httpClient: HttpClient) { }

  public  getPostsForTeam(teamId: number, page: string) {

   let params = new HttpParams().append("page", page)
    return this.httpClient.get<Post[]>(this.url+"/team/"+teamId, {params : params});
  }
  public getPostById(id: number) {
    return this.httpClient.get<Post>(this.url+"/"+id, this.httpOptions)
  }

  public createPost(postInJson : String) {
    return this.httpClient.post(this.url, postInJson, this.httpOptions);
  }
}
