import {Component, Input, OnInit} from '@angular/core';
import {Weather} from "../model/weather/Weather";
import {WeatherService} from "../services/weather.service";
import {User} from "../model/User";
import {formatNumber} from "@angular/common";
import {stringify} from "querystring";
import {timestamp} from "rxjs/operators";
import {UserServiceService} from "../services/user-service.service";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  public weather: Weather
  @Input() city: string;
  ł
  icon: String;
  urlIcon: string;
  loggedUser: User
  windUrl = './images/windArrow.png';

  constructor(private  weatherService: WeatherService,
              private  userSewrvice : UserServiceService) {
  }

  ngOnInit(): void {
    this.getUser()
    this.setWeather()
  }

  getUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    this.loggedUser = JSON.parse(loggedUserFromSession);
  }
    setWeather() {
    console.log(this.loggedUser.city)
    if (this.loggedUser.city != null) this.city = this.loggedUser.city;
    else this.city = "Katowice"
    this.weatherService.getWeatherFor(this.city)
      .subscribe(weather => {

          (this.weather = weather)
          this.icon = (String)(weather.icon);
          this.urlIcon = 'http://openweathermap.org/img/wn/' + this.icon + '@2x.png'
        }
      );
  }

  temp(temp: number) {
    return Math.round(temp - 273)
  }

  getImage() {
    return this.urlIcon;
  }

  changeCity(newCity : string) {
    console.log(newCity);
    if(newCity.length>0 && newCity !=this.city) {
      this.loggedUser.city =  newCity;
      console.log(this.loggedUser);
      this.userSewrvice.updateWeatherInUser(this.loggedUser).subscribe(user => {
  sessionStorage.setItem("loggedUser", JSON.stringify(this.loggedUser));
  this.setWeather();
})
  }
}

}
