import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-comment-view',
  templateUrl: './comment-view.component.html',
  styleUrls: ['./comment-view.component.css']
})
export class CommentViewComponent implements OnInit {
 @Input() public  comment : Comment

  constructor() { }

  ngOnInit(): void {
  }

}
