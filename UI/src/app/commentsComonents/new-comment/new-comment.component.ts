import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CommentService} from "../../services/comment.service";

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.css']
})
export class NewCommentComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private  commentService : CommentService
  ) { }

  ngOnInit(): void {
  }

  addComment(content: string) {
    const postId = +this.route.snapshot.paramMap.get('id')
    let authorLogin  = this.getLoggedUser().username;
    let comment = {content, authorLogin, postId };
    let commentInJson = JSON.stringify(comment);
    this.commentService.createComment(commentInJson)
      .subscribe();


  }

  getLoggedUser() {
    let loggedUserFromSession = sessionStorage.getItem("loggedUser");
    let loggedUser = JSON.parse(loggedUserFromSession);
    return loggedUser
  }
}
