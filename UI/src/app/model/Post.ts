import {Team} from "./Team";
import DateTimeFormat = Intl.DateTimeFormat;

export  class Post {
  id: number
  topic: String;
  content: String;
  authorUserName: string;
  crateDateTime: DateTimeFormat;
  team : Team;
  comments: Comment[];
}
