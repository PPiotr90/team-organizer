import {Role} from "./Role";
import {Link} from "./Link";
import {Note} from "./Note";
import {Team} from "./Team";

export class  User {

  id: number;
  username: String;
  password: String;
  role: String;
  links: Link[];
  notes: Note[];
  team : Team;
  city: string;


}
