import {Post} from "./Post";
import DateTimeFormat = Intl.DateTimeFormat;

export  class Comment {
  id: number;
  post : Post;
  authorUsername: String;
  createDateTime: DateTimeFormat;
  content: String;


}
