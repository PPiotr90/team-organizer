import {User} from "./User";

export  class Registry {
  id : number;
  user: User;
  hours : number;
  date : Date;
  action: String;
}
