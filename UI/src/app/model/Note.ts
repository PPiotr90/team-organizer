export  class  Note{
  id: number;
  topic: String;
  content: String;
  show: boolean;
}
