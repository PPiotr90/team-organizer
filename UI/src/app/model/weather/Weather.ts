import {State} from "./State";
import {Temperature} from "./Temperature";
import {Coordinates} from './Coordinates'
import {Wind} from "./Wind";
import {Snow} from "./Snow";
import {Clouds} from "./Clouds";
import {Information} from "./Information";
import {Rain} from "./Rain";

export class Weather {

  temperature: Temperature;
  visibility: number;
  wind: Wind;
  snow: Snow;
  rain : Rain;
  clouds: Clouds;
  city : String
  icon: number;
  country : String
  description : String

}
