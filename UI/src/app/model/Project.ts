export class Project {
  id: number;
  topic: String;
  number: String;
  finishDate: Date;
  description: String;
  leader: String

}
export enum ProjectPrefix {
  RX=0,
  RZ,
  OW,
  PP,
  PB
}
