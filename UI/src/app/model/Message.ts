import DateTimeFormat = Intl.DateTimeFormat;

export class Message  {
  id: number;
  sender: String;
  receiver: String;
  topic: String;
  content: String;
  sendTime: DateTimeFormat;
  receiverShow: boolean;
  senderShow: boolean;
  read: boolean;


}
