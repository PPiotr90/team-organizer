package pl.pp.back.component.links;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.pp.back.component.commons.AbstractDto;

@Data
@AllArgsConstructor
public class LinkDto extends AbstractDto {
    private String name;
    private String content;

}
