package pl.pp.back.component.commons;

import lombok.AllArgsConstructor;
import lombok.Data;


import javax.persistence.*;


@Entity
@Table
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbstractEntity(Long id) {
        this.id = id;
    }

    public AbstractEntity() {
    }
}
