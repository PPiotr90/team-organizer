package pl.pp.back.component.registries;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

import java.time.LocalDate;

@Component
public class RegistryBuilder extends AbstractBuilder<RegistryDto, Registry> {
    private Registry result;

    public RegistryBuilder setHours(int hours) {
        result.setHours(hours);
        return this;
    }

    public RegistryBuilder setAction(String action) {
        ActionTypes actionToSave = ActionTypes.valueOf(action);
        result.setAction(actionToSave);
        return this;
    }

    public RegistryBuilder setDate(LocalDate date) {
        result.setDate(date);
        return this;
    }

    @Override
    public Registry build() {
        result = new Registry();
        return result;
    }

    @Override
    public Registry buildEntityFromDto(RegistryDto source) {
        return setAction(source.getAction())
                .setDate(source.getDate())
                .setHours(source.getHours())
                .build();
    }
}
