package pl.pp.back.component.registries;

import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/registries")
public class RegistryController extends AbstractController<Registry> {

    private RegistryService registryService;

    public RegistryController(RegistryService registryService) {
        this.registryService = registryService;
    }

    @GetMapping
    @Override
    public List<Registry> getAll() {
        return registryService.findAll();
    }

    @GetMapping("/{id}")
    @Override
    public Registry getById(@PathVariable("id") Long id) {
        return registryService.findById(id);
    }

    @GetMapping("/user/{username}")
    public List<Registry> findByUserName(@PathVariable("username") String username) {
        return registryService.findByUsername(username);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        registryService.deleteById(id);

    }

    @PutMapping
    @Override
    public Registry put(Registry registry) {
        return registryService.save(registry);
    }

    @PostMapping
    public Registry post(RegistryDto registryDto) {
        return registryService.create(registryDto);
    }
}
