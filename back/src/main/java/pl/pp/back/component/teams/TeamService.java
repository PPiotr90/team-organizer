package pl.pp.back.component.teams;

import org.springframework.stereotype.Service;
import pl.pp.back.component.teams.TeamDto;
import pl.pp.back.component.teams.Team;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.teams.TeamRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TeamService {
    private TeamRepository teamRepository;
    private EntityFactory entityFactory;

    TeamService(TeamRepository teamRepository,
                EntityFactory entityFactory) {
        this.teamRepository = teamRepository;
        this.entityFactory = entityFactory;
    }

    public Team getById(Long id) {
        return teamRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in base is no Team with this id"));
    }

    public List<Team> getAll() {
        return teamRepository.findAll();
    }

    public Team save(Team team) {
        return teamRepository.save(team);
    }

    public Team save(TeamDto teamDto) {
        Team teamToSave = entityFactory.createEntity(teamDto);
        return teamRepository.save(teamToSave);

    }

    public void deleteById(Long id) {
        teamRepository.deleteById(id);
    }

}
