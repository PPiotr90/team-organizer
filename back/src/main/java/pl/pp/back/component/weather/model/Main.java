package pl.pp.back.component.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Main implements Serializable {
    private float temp;
    @JsonProperty("feels_like")
    private float feelsLike;
    @JsonProperty("temp_min")
    private float tempMin;
    @JsonProperty("temp_max")
    private float tempMax;
    private long pressure;
    private int humidity;
    @JsonProperty("sea_level")
    private int SeaLevel;
    @JsonProperty("grnd_level")
    private int GandLevel;

}
