package pl.pp.back.component.comments;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.pp.back.component.comments.Comment;
import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.posts.Post;

import java.util.List;

public interface CommentRepository extends AbstractRepository<Comment> {

    @Query("SELECT c FROM  Comment  c WHERE c.id IN :param")
    List<Comment> getAllByCommentIdIn(@Param("param") List<Long> commentsId);

    List<Comment> findAllByPost(Post post);
}
