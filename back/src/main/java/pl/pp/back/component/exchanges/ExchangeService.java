package pl.pp.back.component.exchanges;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.pp.back.component.exchanges.exchangesRates.ExchangesRatesTable;
import pl.pp.back.component.exchanges.exchangesRates.Rate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Service
public class ExchangeService {

    private JsonMapper mapper = new JsonMapper();
    @Value("${exchanges.url}")
    private String exchangesUrl;

    public List<Rate> getRates() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(exchangesUrl))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        String exchangeRatesTableInString = response.body();
        System.out.println(exchangeRatesTableInString);
        ExchangesRatesTable[] exchangesRatesTableList = mapper.readValue(exchangeRatesTableInString, ExchangesRatesTable[].class);
        System.out.println(exchangesRatesTableList);
        return exchangesRatesTableList[0].getRates();

    }
}
