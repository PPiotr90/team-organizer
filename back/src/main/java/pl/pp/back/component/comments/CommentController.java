package pl.pp.back.component.comments;

import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/comments")
public class CommentController extends AbstractController<Comment> {

    private CommentService commentService;

    CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    @GetMapping
    public List<Comment> getAll() {
        return null;
    }

    @Override
    @GetMapping("/{id}")
    public Comment getById(@PathVariable("id") Long id) {
        return null;
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {

    }

    @Override
    @PutMapping
    public Comment put(@RequestBody Comment comment) {
        return commentService.save(comment);
    }

    @PostMapping
    public Comment post(@RequestBody CommentDto commentDto) {
        return commentService.create(commentDto);
    }
}
