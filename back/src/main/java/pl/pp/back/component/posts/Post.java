package pl.pp.back.component.posts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.comments.Comment;
import pl.pp.back.component.commons.AbstractEntity;
import pl.pp.back.component.teams.Team;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
public class Post extends AbstractEntity {
    private String topic;
    @Lob
    private String content;

    private String authorUserName;
    private LocalDateTime crateDateTime;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    @JsonIgnore
    private Team team;

    @OneToMany(mappedBy = "post",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Comment> comments = new ArrayList<>();

    public Post(Long id, String topic, String content, String authorUserName, LocalDateTime crateDate, List<Comment> comments) {
        super(id);
        this.topic = topic;
        this.content = content;
        this.authorUserName = authorUserName;
        this.crateDateTime = crateDate;
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
        comment.setPost(this);
    }
}
