package pl.pp.back.component.projects;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

//TODO controllers for projects, entries as list of projects,

@CrossOrigin("*")
@RequestMapping("/projects")
@RestController
public class ProjectController extends AbstractController<Project> {

    private final ProjectService projectService;

    ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Project> getAll() {
        return projectService.findAll();
    }

    @Override
    public Project getById(Long id) {
        return projectService.getByID(id);
    }

    @Override
    public void deleteById(Long id) {
        projectService.deleteById(id);
    }

    @Override
    public Project put(Project project) {
        return projectService.save(project);
    }

    @PostMapping
    public Project post(@RequestBody ProjectDto projectDto) {
        return projectService.save(projectDto);

    }


}
