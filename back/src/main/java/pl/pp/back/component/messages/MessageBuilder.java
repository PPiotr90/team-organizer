package pl.pp.back.component.messages;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;


import java.time.LocalDateTime;


@Component
public class MessageBuilder extends AbstractBuilder<MessageDto, Message> {
    private Message message;


    public MessageBuilder() {
        super();
        this.message = new Message();
    }

    public MessageBuilder setTopic(String topic) {
        this.message.setTopic(topic);
        return this;
    }

    public MessageBuilder setContent(String content) {
        this.message.setContent(content);
        return this;
    }

    public MessageBuilder setSender(String sender) {
        this.message.setSender(sender);
        return this;
    }

    public MessageBuilder setReceiver(String receiver) {
        this.message.setReceiver(receiver);
        return this;
    }

    public Message build() {
        this.message.setSendTime(LocalDateTime.now().withNano(0));
        this.message.setSenderShow(true);
        this.message.setReceiverShow(true);
        this.message.setRead(false);
        return this.message;
    }

    public Message buildEntityFromDto(MessageDto messageDto) {
        message = new Message();
        return this.setContent(messageDto.getContent())
                .setTopic(messageDto.getTopic())
                .setSender(messageDto.getSender())
                .setReceiver(messageDto.getReceiver())
                .build();
    }
}

