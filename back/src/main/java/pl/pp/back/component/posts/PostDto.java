package pl.pp.back.component.posts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDto extends AbstractDto {
    private String topic;
    private String content;
    private String authorLogin;
    private Long teamId;


}
