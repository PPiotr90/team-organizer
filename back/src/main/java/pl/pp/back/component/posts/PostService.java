package pl.pp.back.component.posts;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.pp.back.component.comments.Comment;
import pl.pp.back.component.teams.Team;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.comments.CommentRepository;
import pl.pp.back.component.teams.TeamService;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostService {
    private PostRepository postRepository;
    private EntityFactory entityFactory;
    private TeamService teamService;
    private CommentRepository commentRepository;

    public PostService(PostRepository postRepository,
                       EntityFactory entityFactory,
                       TeamService teamService,
                       CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.entityFactory = entityFactory;
        this.teamService = teamService;
        this.commentRepository = commentRepository;
    }

    public Post findById(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in db is no post with this id"));
        List<Comment> commentsByPost = commentRepository.findAllByPost(post);
        post.setComments(commentsByPost);
        return post;
    }

    public List<Post> findAll() {
        return postRepository.findAll();
    }

    public Post save(Post post) {
        return postRepository.save(post);
    }

    public Post create(PostDto postDto) {

        Post postFromDto = entityFactory.createEntity(postDto);

        Team team = teamService.getById(postDto.getTeamId());
        postFromDto.setTeam(team);
        return postRepository.save(postFromDto);
    }

    public void deleteById(Long id) {
        postRepository.deleteById(id);
    }

    public List<Post> findByTeamId(Long teamId, int page, int size) {
        List<Post> result = new ArrayList<>();
        postRepository.findAllByTeamId(teamId, PageRequest.of(page - 1, size))
                .stream()
                .forEach(post -> {
                    post.setTeam(null);
                    post.setComments(new ArrayList<>());
                    result.add(post);
                });
        return result;
    }

}
