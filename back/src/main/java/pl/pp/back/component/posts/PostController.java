package pl.pp.back.component.posts;

import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/posts")
public class PostController extends AbstractController<Post> {

    private PostService postService;

    PostController(PostService postService) {
        this.postService = postService;
    }

    @Override
    @GetMapping
    public List<Post> getAll() {
        return postService.findAll();
    }

    @Override
    @GetMapping("/{id}")
    public Post getById(@PathVariable("id") Long id) {
        return postService.findById(id);
    }

    @GetMapping("/team/{teamId}")
    public List<Post> getForTeam(@PathVariable("teamId") Long teamId,
                                 @RequestParam(required = false, defaultValue = "0", name = "page") int page,
                                 @RequestParam(required = false, defaultValue = "5", name = "size") int size) {
        page = page >= 0 ? page : 0;
        size = size >= 0 ? size : 5;
        return postService.findByTeamId(teamId, page, size);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        postService.deleteById(id);

    }

    @Override
    @PutMapping
    public Post put(@RequestBody Post post) {
        return postService.save(post);
    }

    @PostMapping
    public Post post(@RequestBody PostDto postDto) {

        System.out.println(postDto);
        return postService.create(postDto);
    }
}
