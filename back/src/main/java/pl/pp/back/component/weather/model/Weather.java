package pl.pp.back.component.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
public class Weather implements Serializable {
    @JsonProperty("coord")
    private Coordinates coordinates;
    @JsonProperty("weather")
    private List<State> states;
    @JsonProperty("main")
    private Main main;
    private Long visibility;
    private Wind wind;

    private Snow snow;

    private Rain rain;
    private Clouds clouds;
    private String base;
    @JsonProperty("sys")
    private Information information;
    private Long timezone;
    private Long id;
    @JsonProperty("name")
    private String city;
    private String cod;

    private long dt;

}
