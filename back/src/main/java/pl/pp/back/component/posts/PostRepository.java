package pl.pp.back.component.posts;

import org.springframework.data.domain.Pageable;

import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.posts.Post;

import java.util.List;

public interface PostRepository extends AbstractRepository<Post> {


    List<Post> findAllByTeamId(Long teamId, Pageable pageable);
}
