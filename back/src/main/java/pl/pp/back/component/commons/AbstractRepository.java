package pl.pp.back.component.commons;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pp.back.component.commons.AbstractEntity;

public interface AbstractRepository<T extends AbstractEntity> extends JpaRepository<T, Long> {
}
