package pl.pp.back.component.posts;


import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import pl.pp.back.component.comments.Comment;
import pl.pp.back.component.comments.CommentRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostFacade {

    private PostRepository postRepository;
    private CommentRepository commentRepository;

    PostFacade(
            PostRepository postRepository,
            CommentRepository commentRepository) {

        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    public List<Post> findPostForTeam(Long teamId, int page, int size) {
        List<Post> posts = postRepository.findAllByTeamId(teamId,
                PageRequest.of(page, size));

        List<Comment> comments = new ArrayList<>();

        posts.forEach(post -> post.setComments(selectCommentFor(comments, post)));

        return posts;
    }

    private List<Comment> selectCommentFor(List<Comment> comments, Post post) {
        return comments.stream()
                .filter(comment -> comment.getPost().equals(post))
                .collect(Collectors.toList());
    }
}
