package pl.pp.back.component.links;

import org.springframework.stereotype.Service;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.links.LinkRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LinkService {
    private LinkRepository linkRepository;
    private EntityFactory entityFactory;

    public LinkService(LinkRepository linkRepository,
                       EntityFactory entityFactory) {
        this.linkRepository = linkRepository;
        this.entityFactory = entityFactory;
    }

    public void deleteById(Long id) {
        linkRepository.deleteById(id);
    }

    public List<Link> findAll() {
        return linkRepository.findAll();
    }

    public Link findById(Long id) {
        return linkRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in data base is no link with this id"));
    }

    public Link save(Link link) {
        return linkRepository.save(link);
    }

    public Link create(LinkDto linkDto) {
        Link linkToSave = entityFactory.createEntity(linkDto);
        return linkRepository.save(linkToSave);

    }

}
