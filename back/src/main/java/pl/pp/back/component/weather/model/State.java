package pl.pp.back.component.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class State implements Serializable {
    private Long id;
    private String main;
    private String description;
    private String icon;
}
