package pl.pp.back.component.comments;

import org.springframework.stereotype.Service;
import pl.pp.back.component.posts.Post;

import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.posts.PostService;
import pl.pp.back.component.users.UserService;

import javax.persistence.EntityNotFoundException;

import java.util.List;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private EntityFactory entityFactory;
    private UserService userService;
    private PostService postService;

    public CommentService(CommentRepository commentRepository,
                          EntityFactory entityFactory,
                          UserService userService,
                          PostService postService) {
        this.commentRepository = commentRepository;
        this.entityFactory = entityFactory;
        this.userService = userService;
        this.postService = postService;
    }

    public Comment findById(Long id) {
        return commentRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in base is no comment with this id"));
    }

    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    public Comment create(CommentDto commentDto) {
        Post post = postService.findById(commentDto.getPostId());
        Comment commentToSave = entityFactory.createEntity(commentDto);
        post.addComment(commentToSave);
        postService.save(post);
        return commentToSave;
    }

    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }

}
