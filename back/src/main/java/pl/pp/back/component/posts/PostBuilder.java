package pl.pp.back.component.posts;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

import java.time.LocalDateTime;

@Component
public class PostBuilder extends AbstractBuilder<PostDto, Post> {


    private Post result = new Post();

    public PostBuilder setTopic(String topic) {
        this.result.setTopic(topic);
        return this;
    }

    public PostBuilder setContent(String content) {
        result.setContent(content);
        return this;
    }

    public PostBuilder setAuthor(String author) {
        result.setAuthorUserName(author);
        return this;
    }


    @Override
    public Post build() {
        LocalDateTime now = LocalDateTime.now();
        now = now.withNano(0);
        result.setCrateDateTime(now);
        return result;
    }

    @Override
    public Post buildEntityFromDto(PostDto source) {
        result = new Post();
        return setTopic(source.getTopic())
                .setContent(source.getContent())
                .setAuthor(source.getAuthorLogin())
                .build();
    }
}
