package pl.pp.back.component.notes;

import org.springframework.stereotype.Service;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.notes.NoteRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class NoteService {
    private NoteRepository noteRepository;
    EntityFactory entityFactory;

    public NoteService(NoteRepository noteRepository,
                       EntityFactory entityFactory) {
        this.noteRepository = noteRepository;
        this.entityFactory = entityFactory;
    }

    public void deleteById(Long id) {
        noteRepository.deleteById(id);
    }

    public List<Note> findAll() {
        return noteRepository.findAll();
    }

    public Note findById(Long id) {
        return noteRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in data base is no note with this id"));
    }

    public Note save(Note note) {
        return noteRepository.save(note);
    }

    public Note create(NoteDto noteDto) {
        Note newNote = entityFactory.createEntity(noteDto);
        return noteRepository.save(newNote);
    }
}
