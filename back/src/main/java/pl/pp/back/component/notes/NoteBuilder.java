package pl.pp.back.component.notes;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

@Component
public class NoteBuilder extends AbstractBuilder<NoteDto, Note> {

    private Note note = new Note();

    @Override
    public Note build() {
        return note;
    }

    public NoteBuilder setTopic(String topic) {
        note.setTopic(topic);
        return this;
    }

    public NoteBuilder setContent(String content) {
        note.setContent(content);
        return this;
    }


    @Override
    public Note buildEntityFromDto(NoteDto source) {
        note = new Note();
        return this.setTopic(source.getTopic())
                .setContent(source.getContent())
                .build();
    }
}
