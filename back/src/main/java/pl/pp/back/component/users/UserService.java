package pl.pp.back.component.users;

import org.springframework.stereotype.Service;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.users.UserDto;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.users.LoginDto;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.users.User;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.users.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class UserService {
    private EntityFactory entityFactory;
    private UserRepository userRepository;

    public UserService(UserRepository userRepository,
                       EntityFactory entityFactory) {
        this.userRepository = userRepository;
        this.entityFactory = entityFactory;
    }

    public List<String> getAllUsernames() {
        return userRepository.findUsernames();
    }

    public String getPasswordByUsername(String username) {
        return (userRepository.getPasswordByUserName(username))
                .orElseThrow(
                        () -> new EntityNotFoundException("no pass for this user"));
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User save(UserDto userDto) {
        User userToSave = entityFactory.createEntity(userDto);
        return userRepository.save(userToSave);
    }


    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(()
                -> new EntityNotFoundException("in base is no entity with this id"));
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("in base is no user with this username: " + username));
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User check(LoginDto loginDto) {
        return userRepository.findByUsernameAndPassword(loginDto.getUsername(), loginDto.getPassword())
                .orElseThrow(() -> new EntityNotFoundException("username and password dont fit "));
    }

    public User addNoteToUser(String username, NoteDto noteDto) {
        User user = this.findByUsername(username);
        Note noteToSave = entityFactory.createEntity(noteDto);
        System.out.println(noteToSave);
        user.addNote(noteToSave);
        return userRepository.save(user);
    }

    public User addLinkToUser(String username, LinkDto linkDto) {
        User user = this.findByUsername(username);
        Link linkToSave = entityFactory.createEntity(linkDto);
        System.out.println(linkToSave);
        user.addLink(linkToSave);
        return userRepository.save(user);
    }


}
