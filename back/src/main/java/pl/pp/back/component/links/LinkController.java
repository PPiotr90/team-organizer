package pl.pp.back.component.links;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/links")
public class LinkController extends AbstractController<Link> {

    private LinkService linkService;

    public LinkController(LinkService linkService) {
        this.linkService = linkService;
    }

    @Override
    public List<Link> getAll() {
        return linkService.findAll();
    }

    @Override
    @GetMapping("/{id}")
    public Link getById(@PathVariable("id") Long id) {
        return linkService.findById(id);
    }

    @Override
    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        linkService.deleteById(id);

    }

    @Override
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping
    public Link put(@RequestBody Link link) {
        return linkService.save(link);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Link post(@RequestBody LinkDto linkDto) {
        return linkService.create(linkDto);
    }
}
