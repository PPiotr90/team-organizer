package pl.pp.back.component.notes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoteDto extends AbstractDto {

    private String topic;

    private String content;
}
