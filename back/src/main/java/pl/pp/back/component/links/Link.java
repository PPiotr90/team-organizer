package pl.pp.back.component.links;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;

import javax.persistence.Entity;

@Data
@Entity

@NoArgsConstructor
public class Link extends AbstractEntity {

    private String name;
    private String content;

    public Link(Long id,
                String name,
                String content) {
        super(id);
        this.name = name;
        this.content = content;
    }
}
