package pl.pp.back.component.teams;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

@Component
public class TeamBuilder extends AbstractBuilder<TeamDto, Team> {
    private Team result = new Team();

    TeamBuilder setName(String name) {
        this.result.setName(name);
        return this;
    }

    @Override
    public Team build() {
        return result;
    }

    @Override
    public Team buildEntityFromDto(TeamDto source) {
        result = new Team();
        return setName(source.getName())
                .build();
    }
}
