package pl.pp.back.component.notes;

import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.notes.Note;

public interface NoteRepository extends AbstractRepository<Note> {
}
