package pl.pp.back.component.projects;

import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.projects.Project;

public interface ProjectRepository extends AbstractRepository<Project> {

}
