package pl.pp.back.component.registries;

import org.springframework.stereotype.Service;
import pl.pp.back.component.registries.RegistryDto;
import pl.pp.back.component.registries.Registry;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.registries.RegistryRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class RegistryService {
    private RegistryRepository registryRepository;
    EntityFactory entityFactory;

    public RegistryService(RegistryRepository registryRepository,
                           EntityFactory entityFactory) {
        this.registryRepository = registryRepository;
        this.entityFactory = entityFactory;
    }

    public List<Registry> findAll() {
        return registryRepository.findAll();
    }

    public Registry findById(Long id) {
        return registryRepository.findById(id)
                .orElseThrow(()
                        -> new EntityNotFoundException("there is no registry with id: " + id));
    }

    public Registry create(RegistryDto registryDto) {
        Registry entityFromBuilder = entityFactory.createEntity(registryDto);
        return registryRepository.save(entityFromBuilder);

    }

    public Registry save(Registry registry) {
        return registryRepository.save(registry);
    }

    public void deleteById(Long id) {
        registryRepository.deleteById(id);
    }

    public List<Registry> findByUsername(String username) {
        return registryRepository.findAllByUsername(username);
    }
}
