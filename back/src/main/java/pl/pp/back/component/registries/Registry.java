package pl.pp.back.component.registries;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;
import pl.pp.back.component.users.User;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
@Table
public class Registry extends AbstractEntity {


    @ManyToOne()
    private User user;
    private int hours;
    private LocalDate date;
    @Enumerated
    private ActionTypes action;

    public Registry(Long id,
                    User user,
                    int hours,
                    LocalDate date,
                    ActionTypes action) {
        super(id);
        this.user = user;
        this.hours = hours;
        this.date = date;
        this.action = action;
    }
}
