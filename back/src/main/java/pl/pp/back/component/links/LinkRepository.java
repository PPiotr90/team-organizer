package pl.pp.back.component.links;

import org.springframework.stereotype.Repository;
import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.links.Link;

@Repository
public interface LinkRepository extends AbstractRepository<Link> {
}
