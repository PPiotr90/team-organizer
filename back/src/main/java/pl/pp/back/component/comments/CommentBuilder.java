package pl.pp.back.component.comments;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

import java.time.LocalDateTime;

@Component
public class CommentBuilder extends AbstractBuilder<CommentDto, Comment> {


    private Comment result = new Comment();


    public CommentBuilder setContent(String content) {
        this.result.setContent(content);
        return this;
    }

    public CommentBuilder setAuthorUserName(String authorUserName) {
        result.setAuthorUsername(authorUserName);
        return this;
    }


    @Override
    public Comment build() {
        LocalDateTime now = LocalDateTime.now();
        now = now.withNano(0);
        result.setCreateDateTime(now);
        return result;
    }

    @Override
    public Comment buildEntityFromDto(CommentDto source) {
        result = new Comment();
        return this.setContent(source.getContent())
                .setAuthorUserName(source.getAuthorLogin())
                .build();
    }
}
