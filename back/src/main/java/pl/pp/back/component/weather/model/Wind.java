package pl.pp.back.component.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Wind implements Serializable {
    private float speed;
    @JsonProperty("deg")
    private int direction;
    private float gust;
}
