package pl.pp.back.component.teams;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;

import javax.persistence.Entity;


@Data
@Entity
@NoArgsConstructor
public class Team extends AbstractEntity {
    private Long id;
    private String name;


    public Team(Long id, Long id1, String name) {
        super(id);
        this.id = id1;
        this.name = name;

    }
}
