package pl.pp.back.component.commons;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public abstract class AbstractController<T extends AbstractEntity> {

    @GetMapping
    public abstract List<T> getAll();

    @GetMapping("/{id}")
    public abstract T getById(@PathVariable("id") Long id);

    @DeleteMapping("/{id}")
    public abstract void deleteById(@PathVariable("id") Long id);

    @PutMapping
    public abstract T put(@RequestBody T t);


}
