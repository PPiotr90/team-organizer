package pl.pp.back.component.messages;

import org.springframework.stereotype.Service;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.messages.Message;
import pl.pp.back.component.messages.MessageRepository;
import pl.pp.back.component.messages.MessageDto;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@Service
public class MessageService {
    private EntityFactory entityFactory;
    private MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository,
                          EntityFactory entityFactory) {
        this.messageRepository = messageRepository;
        this.entityFactory = entityFactory;
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public List<Message> findAllBySender(String sender) {
        return messageRepository.getAllBySender(sender);
    }

    public List<Message> findAllByReceiver(String receiver) {
        return messageRepository.getAllByReceiver(receiver);
    }

    public Message save(MessageDto messageDto) {
        Message massageToSave = entityFactory.createEntity(messageDto);
        return messageRepository.save(massageToSave);
    }

    public Long countUnreadMessageByReceiver(String username) {
        return messageRepository.getNumberOfUnReadMessageByReceiver(username);
    }

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public Message findById(Long id) {
        return messageRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("in base is no message with this id"));
    }

    public Message setAsRead(Long id) {
        Message readMessage = this.findById(id);
        readMessage.setRead(true);
        return this.messageRepository.save(readMessage);
    }

    public Integer deleteForReceiver(List<Long> ids) {
        if (ids.size() > 0) {
            return this.messageRepository.setDeleteForReceiver(ids);
        } else return null;
    }

    public Integer deleteForSender(List<Long> ids) {
        if (ids.size() > 0) {
            return this.messageRepository.setDeleteForSender(ids);
        } else return null;

    }
}
