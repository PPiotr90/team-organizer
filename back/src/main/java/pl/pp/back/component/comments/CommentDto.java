package pl.pp.back.component.comments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto extends AbstractDto {
    private String content;
    private String authorLogin;
    private Long postId;

}
