package pl.pp.back.component;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.pp.back.component.comments.CommentDto;
import pl.pp.back.component.posts.Post;
import pl.pp.back.component.posts.PostDto;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.teams.Team;
import pl.pp.back.component.teams.TeamDto;
import pl.pp.back.component.users.RoleTypes;
import pl.pp.back.component.users.User;
import pl.pp.back.component.projects.ProjectBuilder;
import pl.pp.back.component.projects.ProjectRepository;
import pl.pp.back.component.users.UserRepository;
import pl.pp.back.component.comments.CommentService;
import pl.pp.back.component.posts.PostService;
import pl.pp.back.component.teams.TeamService;

import java.time.LocalDate;
import java.util.List;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private UserRepository userRepository;
    private ProjectRepository projectRepository;
    private ProjectBuilder projectBuilder;
    private PostService postService;
    private TeamService teamService;
    private CommentService commentService;

    DataInitializer(UserRepository userRepository,
                    ProjectRepository projectRepository,
                    ProjectBuilder projectBuilder,
                    PostService postService,
                    TeamService teamService,
                    CommentService commentService) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.projectBuilder = projectBuilder;
        this.postService = postService;
        this.teamService = teamService;
        this.commentService = commentService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        for (int i = 0; i < 20; i++) {

            User user = new User("user" + i, "123");
            if (i == 0) user.setRole(RoleTypes.DIRECTOR);
            userRepository.save(user);

            Project project = new Project(null,
                    "topic" + i,
                    "project" + i,
                    LocalDate.of(2021, i % 12 + 1, 1),
                    "there is very long description number: " + i * 2);
            projectRepository.save(project);
        }

        for (int teamId = 0; teamId < 5; teamId++) {
            TeamDto teamDto = new TeamDto("team " + teamId);


            Team team = teamService.save(teamDto);
        }
        List<Team> allTeams = teamService.getAll();
        allTeams.stream().forEach(team -> {
            for (int postId = 0; postId < 10; postId++) {


                PostDto postDto = new PostDto("topic " + postId,
                        "content: " + postId
                                + "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " + "\n" +
                                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " + "\n" +
                                "when an unknown printer took a galley of type and scrambled it to make a type " + "\n" +
                                "specimen book. It has survived not only five centuries, but also the leap into " + "\n" +
                                "electronic typesetting, remaining essentially unchanged. It was popularised in " + "\n" +
                                "the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and " + "\n" +
                                "more recently with desktop publishing software like Aldus PageMaker including versions " + "\n" +
                                "of Lorem Ipsum.",
                        "user" + postId,
                        team.getId());
                Post post = postService.create(postDto);

                for (int commentId = 0; commentId < 10; commentId++) {
                    CommentDto commentDto = new CommentDto("content: " + postId + commentId,
                            "user0",
                            post.getId());

                    commentService.create(commentDto);
                }
                int i = 0;
                List<User> users = userRepository.findAll();
                for (User user : users) {
                    user.setTeam(allTeams.get(i % 4));
                    i++;
                    userRepository.save(user);
                }
            }
        });


    }
}
