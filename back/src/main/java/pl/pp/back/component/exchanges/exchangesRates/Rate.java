package pl.pp.back.component.exchanges.exchangesRates;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rate implements Serializable {
    private String currency;
    private String code;
    private double bid;
    private double ask;
}
