package pl.pp.back.component.weather;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.pp.back.component.weather.model.Weather;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class WeatherService {
    @Value("${weather.url}")
    private String weatherUrl;
    @Value("${x-rapid-key}")
    private String xRapidKey;

    @Value("${x-rapid.host}")
    private String xRapidHost;

    JsonMapper mapper = new JsonMapper();

    public WeatherDto getWeatherDto(String city) throws IOException, InterruptedException {
        {
            String weatherCityUrl = weatherUrl.replace("city", city);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(weatherCityUrl))
                    .header("x-rapidapi-key", xRapidKey)
                    .header("x-rapidapi-host", xRapidHost)
                    .method("GET", HttpRequest.BodyPublishers.noBody())
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            String weatherInString = response.body();
            weatherInString = weatherInString.substring(8, weatherInString.length() - 1);
            System.out.println(weatherInString);
            Weather weather = mapper.readerFor(Weather.class).readValue(weatherInString);
            System.out.println(weather);

            WeatherDto result = mapWeatherToDto(weather);
            return result;
        }
    }

    private WeatherDto mapWeatherToDto(Weather source) {
        WeatherDto result = new WeatherDto();
        result.setCity(source.getCity());
        result.setClouds(source.getClouds());
        result.setRain(source.getRain());
        result.setMain(source.getMain());
        result.setSnow(source.getSnow());
        result.setWind(source.getWind());
        result.setIcon(source.getStates().get(0).getIcon());
        result.setCountry(source.getInformation().getCountry());
        result.setDescription(source.getStates().get(0).getDescription());
        return result;
    }

}
