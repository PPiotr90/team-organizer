package pl.pp.back.component.users;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.notes.NoteDto;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/users")
public class UserController extends AbstractController<User> {
    private final UserService userService;

    UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAll() {
        return this.userService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User post(@RequestBody UserDto userDto) {
        return userService.save(userDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User getById(
            @PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @Override
    @DeleteMapping("{id}")
    public void deleteById(@PathVariable("id") Long id) {
        userService.deleteById(id);
    }

    @Override
    @PutMapping
    public User put(@RequestBody User user) {
        return userService.save(user);

    }


    @GetMapping("/pass/{username}")
    @ResponseStatus(HttpStatus.OK)
    public String getPasswordForUser(
            @PathVariable("username") String username) {
        return userService.getPasswordByUsername(username);
    }

    @GetMapping("/usernames")
    @ResponseStatus(HttpStatus.OK)
    public List<String> getUsernames() {
        return userService.getAllUsernames();
    }


    @GetMapping("/check")
    public User checkLoginInputs(@RequestBody LoginDto loginDto) {
        return userService.check(loginDto);
    }

    @GetMapping("/username/{username}")
    public User getByUsername(@PathVariable("username") String username) {
        return userService.findByUsername(username);
    }

    @PostMapping("/{username}/addNote")
    public User addNotice(@PathVariable(value = "username") String username,
                          @RequestBody() NoteDto noteDto) {
        return userService.addNoteToUser(username, noteDto);
    }

    @PostMapping("/{username}/addLink")
    public User addLink(@PathVariable(value = "username") String username,
                        @RequestBody() LinkDto linkDto) {
        return userService.addLinkToUser(username, linkDto);
    }
}
