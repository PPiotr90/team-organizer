package pl.pp.back.component.teams;

import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/teams")
public class TeamController extends AbstractController<Team> {
    private TeamService teamService;

    TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    @GetMapping
    public List<Team> getAll() {
        return teamService.getAll();
    }

    @Override
    @GetMapping("/{id}")
    public Team getById(@PathVariable("id") Long id) {
        return teamService.getById(id);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        teamService.deleteById(id);
    }

    @Override
    @PutMapping
    public Team put(@RequestBody Team team) {
        return teamService.save(team);
    }

    @PostMapping
    public Team post(@RequestBody TeamDto teamDto) {
        return teamService.save(teamDto);
    }
}
