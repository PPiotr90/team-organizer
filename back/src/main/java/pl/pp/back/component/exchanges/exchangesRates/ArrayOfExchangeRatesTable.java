package pl.pp.back.component.exchanges.exchangesRates;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArrayOfExchangeRatesTable implements Serializable {
    private List<ExchangesRatesTable> exchangesRatesTableList;
}
