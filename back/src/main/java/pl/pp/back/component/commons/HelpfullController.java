package pl.pp.back.component.commons;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.pp.back.component.users.RoleTypes;

@RestController
@RequestMapping("/")
public class HelpfullController {
    private HelpfulService helpfulService;

    HelpfullController(HelpfulService roleService) {
        this.helpfulService = roleService;
    }

    @GetMapping("/roles")
    @ResponseStatus(HttpStatus.OK)
    public RoleTypes[] getRoles() {
        return helpfulService.getRoles();
    }


}
