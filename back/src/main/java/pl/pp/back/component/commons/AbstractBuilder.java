package pl.pp.back.component.commons;

import pl.pp.back.component.commons.AbstractDto;
import pl.pp.back.component.commons.AbstractEntity;

public abstract class AbstractBuilder<Dto extends AbstractDto, E extends AbstractEntity> {

    E result;

    public AbstractBuilder() {

    }


    public abstract E build();

    public abstract E buildEntityFromDto(Dto dto);
}
