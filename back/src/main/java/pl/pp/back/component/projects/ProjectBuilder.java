package pl.pp.back.component.projects;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Component
public class ProjectBuilder extends AbstractBuilder<ProjectDto, Project> {

    private Project project;

    public ProjectBuilder() {
        project = new Project();
    }


    public ProjectBuilder setFinishDate(LocalDate localDate) {
        project.setFinishDate(localDate);
        return this;
    }

    public ProjectBuilder setTopic(String topic) {
        project.setTopic(topic);
        return this;
    }

    public ProjectBuilder setDescription(String description) {
        project.setDescription(description);
        return this;
    }

    public static Project setNumber(Project projectToChange, String projectPrefix) {
        String number = projectPrefix + "-" + projectToChange.getId();
        projectToChange.setNumber(number);
        return projectToChange;
    }

    public ProjectBuilder setLeader(String leader) {
        project.setLeader(leader);
        return this;
    }

    @Override
    public Project build() {
        return project;

    }

    @Transactional
    @Override
    public Project buildEntityFromDto(ProjectDto source) {
        project = new Project();
        return this.setDescription(source.getDescription())
                .setFinishDate(source.getFinishDate())
                .setTopic(source.getTopic())
                .setLeader(source.getLeader())
                .build();

    }
}
