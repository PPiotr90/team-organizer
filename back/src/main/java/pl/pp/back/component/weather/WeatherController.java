package pl.pp.back.component.weather;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@CrossOrigin("*")
@RequestMapping("/weather")
public class WeatherController {
    private WeatherService weatherService;

    WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/{city}")
    @ResponseBody
    public WeatherDto getWeatherForCity(@PathVariable("city") String city) throws IOException, InterruptedException {
        return weatherService.getWeatherDto(city);
    }
}
