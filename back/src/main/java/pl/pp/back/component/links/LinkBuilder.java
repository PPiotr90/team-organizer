package pl.pp.back.component.links;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

import javax.transaction.Transactional;

@Component
public class LinkBuilder extends AbstractBuilder<LinkDto, Link> {

    private Link result = new Link();

    public LinkBuilder setName(String name) {
        result.setName(name);
        return this;
    }

    public LinkBuilder setContent(String content) {
        if (!content.startsWith("http://"))
            content = "http://" + content;
        result.setContent(content);
        return this;
    }

    @Override
    public Link build() {
        return result;
    }

    @Transactional
    @Override
    public Link buildEntityFromDto(LinkDto source) {
        result = new Link();
        return setName(source.getName())
                .setContent(source.getContent())
                .build();
    }
}
