package pl.pp.back.component.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.pp.back.component.commons.AbstractDto;


@Data
@AllArgsConstructor
public class MessageDto extends AbstractDto {

    private String topic;
    private String content;
    private String sender;
    public String receiver;


}
