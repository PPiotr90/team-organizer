package pl.pp.back.component.projects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;


import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDto extends AbstractDto {
    private String topic;
    private String projectPrefix;
    private LocalDate finishDate;
    private String Leader;
    private String description;
}
