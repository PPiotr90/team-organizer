package pl.pp.back.component.exchanges.exchangesRates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
public class ExchangesRatesTable implements Serializable {
    private String table;
    @JsonProperty("no")
    private String number;
    private Date tradingDate;
    private Date effectiveDate;
    private List<Rate> rates;

}
