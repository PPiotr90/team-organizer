package pl.pp.back.component.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends AbstractDto {
    private String username;
    private String password;
    private String role;

    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

}
