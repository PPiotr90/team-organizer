package pl.pp.back.component.registries;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.registries.Registry;

import java.util.List;

public interface RegistryRepository extends AbstractRepository<Registry> {


    @Query("SELECT r FROM  Registry  r where  r.user.username = :username")
    List<Registry> findAllByUsername(@Param("username") String username);
}
