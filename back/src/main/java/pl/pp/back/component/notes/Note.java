package pl.pp.back.component.notes;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;

import javax.persistence.Entity;

@Data
@Entity
@NoArgsConstructor
public class Note extends AbstractEntity {


    private String topic;

    private String content;

    public Note(Long id,
                String topic,
                String content) {
        super(id);
        this.topic = topic;
        this.content = content;
    }
}
