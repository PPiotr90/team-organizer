package pl.pp.back.component.messages;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.messages.Message;

import javax.transaction.Transactional;
import java.util.List;

public interface MessageRepository extends AbstractRepository<Message> {


    @Query("SELECT m from  Message m WHERE m.receiver = :receiver AND  m.receiverShow = true")
    public List<Message> getAllByReceiver(
            @Param("receiver") String receiver);

    @Query("SELECT  m  FROM  Message  m WHERE  m.sender = :sender AND m.senderShow = true")
    List<Message> getAllBySender(
            @Param("sender") String sender);

    @Query("SELECT COUNT(*) from  Message m WHERE m.receiver = :receiver AND  m.read = false AND  m.receiverShow=true ")
    Long getNumberOfUnReadMessageByReceiver(
            @Param("receiver") String receiver);

    @Transactional
    @Modifying
    @Query("UPDATE Message m SET m.senderShow=FALSE WHERE m.id IN :ids")
    Integer setDeleteForSender(@Param("ids") List<Long>  ids);

    @Transactional
    @Modifying
    @Query("UPDATE Message m SET m.receiverShow=FALSE WHERE m.id IN :ids")
    Integer setDeleteForReceiver(
            @Param("ids") List<Long> ids);
}
