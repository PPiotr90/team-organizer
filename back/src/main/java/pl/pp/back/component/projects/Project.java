package pl.pp.back.component.projects;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;


import javax.persistence.*;
import java.time.LocalDate;


@Data
@Entity
@NoArgsConstructor
public class Project extends AbstractEntity {


    private String topic;
    private String number;
    private LocalDate finishDate;
    private String description;
    private String leader;

    public Project(Long id,
                   String topic,
                   String number,
                   LocalDate finishDate,
                   String description,
                   String leader) {
        super(id);
        this.topic = topic;
        this.number = number;
        this.finishDate = finishDate;
        this.description = description;
        this.leader = leader;
    }

    public Project(Long id,
                   String topic,
                   String number,
                   LocalDate finishDate,
                   String description) {
        super(id);
        this.topic = topic;
        this.number = number;
        this.finishDate = finishDate;
        this.description = description;
    }


}
