package pl.pp.back.component.messages;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/messages")
public class MessageController extends AbstractController<Message> {

    private MessageService messageService;

    MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Message> getAll() {
        return messageService.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Message getById(@PathVariable("id") Long id) {
        return messageService.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        messageService.deleteById(id);
    }

    @Override
    public Message put(Message message) {
        return messageService.save(message);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Message post(@RequestBody MessageDto messageDto) {
        return messageService.save(messageDto);
    }


    @GetMapping("/sender/{sender}")
    @ResponseStatus(HttpStatus.OK)
    public List<Message> getMessageForSender(
            @PathVariable("sender") String sender) {
        return messageService.findAllBySender(sender);
    }

    @GetMapping("/receiver/{receiver}")
    @ResponseStatus(HttpStatus.OK)
    public List<Message> getMessageForReceiver(
            @PathVariable("receiver") String receiver) {
        return messageService.findAllByReceiver(receiver);
    }

    @GetMapping("/unread/{receiver}")
    public Long countUnreadMessageFor(
            @PathVariable("receiver") String receiver) {
        return messageService.countUnreadMessageByReceiver(receiver);
    }

    @PatchMapping("/read/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Message setAsRead(
            @PathVariable("id") Long id) {
        return messageService.setAsRead(id);
    }

    @PatchMapping("/delete/receiver")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteForReceiver(
            @RequestBody List<Long> ids) {
        messageService.deleteForReceiver(ids);
    }

    @PatchMapping("/delete/sender")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Integer deleteForSender(
            @RequestBody List<Long> ids) {
        return messageService.deleteForSender(ids);
    }
}
