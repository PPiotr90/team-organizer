package pl.pp.back.component.users;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.users.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends AbstractRepository<User> {

    @Query("SELECT u.password FROM User u  WHERE  u.username LIKE :param ")
    Optional<String> getPasswordByUserName(@Param("param") String username);

    @Query("SELECT u.username FROM  User u")
    List<String> findUsernames();

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);

    @Query("UPDATE User  SET city= :city WHERE id= :id")
    User updateCityFor(@Param("id") long id, @Param("city") String city);
}



