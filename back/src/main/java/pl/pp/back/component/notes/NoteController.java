package pl.pp.back.component.notes;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.back.component.commons.AbstractController;

import java.util.List;

@RestController
@RequestMapping("/notes")
@CrossOrigin("*")
public class NoteController extends AbstractController<Note> {

    NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @Override
    @GetMapping
    public List<Note> getAll() {
        return noteService.findAll();
    }

    @Override
    @GetMapping("/{id}")
    public Note getById(@PathVariable("id") Long id) {
        return noteService.findById(id);
    }

    @Override
    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        noteService.deleteById(id);


    }

    @Override
    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Note put(@RequestBody Note note) {
        return noteService.save(note);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Note post(@RequestBody NoteDto noteDto) {

        return noteService.create(noteDto);
    }
}
