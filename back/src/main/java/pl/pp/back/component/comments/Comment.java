package pl.pp.back.component.comments;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;
import pl.pp.back.component.posts.Post;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Comment extends AbstractEntity {

    private String authorUsername;
    private LocalDateTime createDateTime;
    @Lob
    private String content;
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Post post;

    public Comment(Long id,
                   String authorUsername,
                   LocalDateTime createDate,
                   String content,
                   Post post) {
        super(id);
        this.authorUsername = authorUsername;
        this.createDateTime = createDate;
        this.content = content;
        this.post = post;
    }
}
