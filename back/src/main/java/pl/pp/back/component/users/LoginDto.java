package pl.pp.back.component.users;

import lombok.Data;

@Data
public class LoginDto {
    String username;
    String password;
}
