package pl.pp.back.component.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Information implements Serializable {
    private int type;
    private long id;
    private String country;
    private long sunrise;
    private long sunset;
}
