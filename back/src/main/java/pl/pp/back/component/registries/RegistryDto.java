package pl.pp.back.component.registries;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistryDto extends AbstractDto {
    private int hours;
    private LocalDate date;
    private String action;
}
