package pl.pp.back.component.projects;

import org.springframework.stereotype.Service;
import pl.pp.back.component.projects.ProjectDto;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.projects.ProjectRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {
    private ProjectRepository projectRepository;
    private EntityFactory entityFactory;

    public ProjectService(ProjectRepository projectRepository,
                          EntityFactory entityFactory) {
        this.projectRepository = projectRepository;
        this.entityFactory = entityFactory;
    }

    public Project getByID(Long id) {
        return projectRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("there is no entity in data base with this id"));

    }


    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project save(ProjectDto projectDto) {
        Project projectToSave = entityFactory.createEntity(projectDto);
        projectToSave = projectRepository.save(projectToSave);
        projectToSave.setNumber(projectDto.getProjectPrefix() + "-" + projectToSave.getId());

        return projectRepository.save(projectToSave);
    }

    public void deleteById(Long id) {
        projectRepository.deleteById(id);
    }

    public Project save(Project project) {
        return projectRepository.save(project);
    }
}
