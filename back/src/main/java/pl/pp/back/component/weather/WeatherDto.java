package pl.pp.back.component.weather;

import lombok.Data;
import pl.pp.back.component.weather.model.*;

@Data
public class WeatherDto {

    private String city;

    private Main main;

    private Wind wind;

    private Snow snow;

    private Rain rain;
    private Clouds clouds;
    private String icon;
    private String country;
    private String description;
}
