package pl.pp.back.component.users;

import lombok.Data;
import pl.pp.back.component.commons.AbstractEntity;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.teams.Team;
import pl.pp.back.component.users.RoleTypes;

import javax.persistence.*;
import java.util.List;

@Data

@Entity
public class User extends AbstractEntity {
    private String username;
    private String password;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Link> links;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Note> notes;

    @ManyToOne
    @JoinColumn
    private Team team;


    @Enumerated(EnumType.STRING)
    private RoleTypes role;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.role = RoleTypes.NONE;
    }

    public User(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = RoleTypes.NONE;
    }

    public User(Long id, String username, String password, RoleTypes role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public User() {
        this.role = RoleTypes.NONE;
    }

    public void addLink(Link link) {
        this.links.add(link);
    }

    public void addNote(Note note) {
        this.notes.add(note);
    }


    public User(Long id, String username,
                String password,
                List<Link> links,
                List<Note> notes,
                RoleTypes role) {
        super(id);
        this.username = username;
        this.password = password;
        this.links = links;
        this.notes = notes;
        this.role = role;
    }
}

