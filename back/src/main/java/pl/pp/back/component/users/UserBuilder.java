package pl.pp.back.component.users;

import org.springframework.stereotype.Component;
import pl.pp.back.component.commons.AbstractBuilder;

@Component
public class UserBuilder extends AbstractBuilder<UserDto, User> {

    private User user;

    public UserBuilder() {
        this.user = new User();

    }

    public UserBuilder setUsername(String username) {
        this.user.setUsername(username);
        return this;
    }

    public UserBuilder setPassword(String password) {
        this.user.setPassword(password);
        return this;
    }

    public UserBuilder setRole(RoleTypes role) {
        if (role == null) user.setRole(RoleTypes.NONE);
        else this.user.setRole(role);
        return this;
    }

    public UserBuilder setRole(String roleName) {
        if (roleName == null) user.setRole(RoleTypes.NONE);
        else {
            RoleTypes role = RoleTypes.valueOf(roleName);
            return this.setRole(role);
        }
        return this;
    }

    public User build() {
        return user;
    }


    public User buildEntityFromDto(UserDto userDto) {
        user = new User();
        return setUsername(userDto.getUsername())
                .setPassword(userDto.getPassword())
                .setRole(userDto.getRole())
                .build();
    }
}
