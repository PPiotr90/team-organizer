package pl.pp.back.component.users;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
public enum RoleTypes {
    NONE(0L, 0d),
    ASSISTANT(1l, 10d),
    SENIOR_ASSISTANT(2l, 13d),
    DESIGNER(3l, 15d),
    MAIN_DESIGNER(4l, 20d),
    SENIOR_DESIGNER(5l, 24d),
    DIRECTOR(6l, 26d),
    PRESIDENT(7l, 30d);

    @Id
    @GeneratedValue
    private Long id;
    private Double rate;

    RoleTypes(Long id, Double rate) {
        this.id = id;
        this.rate = rate;
    }


    public Long getId() {
        return id;
    }


    public Double getRate() {
        return rate;
    }

}
