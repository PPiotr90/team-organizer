package pl.pp.back.component.teams;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractDto;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeamDto extends AbstractDto {
    private String name;
}
