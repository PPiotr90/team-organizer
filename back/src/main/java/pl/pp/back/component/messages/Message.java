package pl.pp.back.component.messages;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pp.back.component.commons.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data

@NoArgsConstructor
@Entity
public class Message extends AbstractEntity {

    private String topic;
    private String content;
    private LocalDateTime sendTime;
    private boolean senderShow;
    private boolean receiverShow;
    private String sender;
    private String receiver;
    private boolean read;

    public Message(Long id,
                   String topic,
                   String content,
                   LocalDateTime sendTime,
                   boolean senderShow,
                   boolean receiverShow,
                   String sender,
                   String receiver,
                   boolean read) {
        super(id);
        this.topic = topic;
        this.content = content;
        this.sendTime = sendTime;
        this.senderShow = senderShow;
        this.receiverShow = receiverShow;
        this.sender = sender;
        this.receiver = receiver;
        this.read = read;
    }
}
