package pl.pp.back.component.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.pp.back.component.comments.Comment;
import pl.pp.back.component.comments.CommentBuilder;
import pl.pp.back.component.comments.CommentDto;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.links.LinkBuilder;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.messages.Message;
import pl.pp.back.component.messages.MessageBuilder;
import pl.pp.back.component.messages.MessageDto;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.notes.NoteBuilder;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.posts.Post;
import pl.pp.back.component.posts.PostBuilder;
import pl.pp.back.component.posts.PostDto;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.projects.ProjectBuilder;
import pl.pp.back.component.projects.ProjectDto;
import pl.pp.back.component.registries.Registry;
import pl.pp.back.component.registries.RegistryBuilder;
import pl.pp.back.component.registries.RegistryDto;
import pl.pp.back.component.teams.Team;
import pl.pp.back.component.teams.TeamBuilder;
import pl.pp.back.component.teams.TeamDto;
import pl.pp.back.component.users.User;
import pl.pp.back.component.users.UserBuilder;
import pl.pp.back.component.users.UserDto;

@Component
public class EntityFactory {

    private MessageBuilder messageBuilder;
    private UserBuilder userBuilder;
    private ProjectBuilder projectBuilder;
    private LinkBuilder linkBuilder;
    private NoteBuilder noteBuilder;
    private PostBuilder postBuilder;
    private CommentBuilder commentBuilder;
    private TeamBuilder teamBuilder;
    private RegistryBuilder registryBuilder;

    @Autowired
    public EntityFactory(MessageBuilder messageBuilder,
                         UserBuilder userBuilder,
                         ProjectBuilder projectBuilder,
                         LinkBuilder linkBuilder,
                         NoteBuilder noteBuilder,
                         PostBuilder postBuilder,
                         CommentBuilder commentBuilder,
                         TeamBuilder teamBuilder,
                         RegistryBuilder registryBuilder) {
        this.messageBuilder = messageBuilder;
        this.userBuilder = userBuilder;
        this.projectBuilder = projectBuilder;
        this.linkBuilder = linkBuilder;
        this.noteBuilder = noteBuilder;
        this.teamBuilder = teamBuilder;
        this.commentBuilder = commentBuilder;
        this.postBuilder = postBuilder;
        this.registryBuilder = registryBuilder;
    }

    public EntityFactory() {
        messageBuilder = new MessageBuilder();
        userBuilder = new UserBuilder();
        projectBuilder = new ProjectBuilder();
        linkBuilder = new LinkBuilder();
        noteBuilder = new NoteBuilder();
    }

    public Message createEntity(MessageDto messageDto) {
        return messageBuilder.buildEntityFromDto(messageDto);
    }

    public User createEntity(UserDto userDto) {
        return userBuilder.buildEntityFromDto(userDto);
    }

    public Project createEntity(ProjectDto projectDto) {
        return projectBuilder.buildEntityFromDto(projectDto);
    }

    public Link createEntity(LinkDto linkDto) {
        return linkBuilder.buildEntityFromDto(linkDto);
    }

    public Note createEntity(NoteDto noteDto) {
        return noteBuilder.buildEntityFromDto(noteDto);
    }

    public Post createEntity(PostDto postDto) {
        return postBuilder.buildEntityFromDto(postDto);
    }

    public Comment createEntity(CommentDto commentDto) {
        return commentBuilder.buildEntityFromDto(commentDto);
    }

    public Team createEntity(TeamDto teamDto) {
        return teamBuilder.buildEntityFromDto(teamDto);
    }

    public Registry createEntity(RegistryDto registryDto) {
        return registryBuilder.buildEntityFromDto(registryDto);
    }

}
