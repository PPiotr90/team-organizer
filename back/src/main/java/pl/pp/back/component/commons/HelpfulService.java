package pl.pp.back.component.commons;

import org.springframework.stereotype.Service;
import pl.pp.back.component.users.RoleTypes;

@Service
public class HelpfulService {
    public RoleTypes[] getRoles() {
        return RoleTypes.values();
    }

}
