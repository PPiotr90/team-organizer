package pl.pp.back.component.teams;

import pl.pp.back.component.commons.AbstractRepository;
import pl.pp.back.component.teams.Team;

public interface TeamRepository extends AbstractRepository<Team> {
}
