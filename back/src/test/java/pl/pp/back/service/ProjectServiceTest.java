package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.projects.ProjectDto;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.projects.ProjectRepository;
import pl.pp.back.component.projects.ProjectService;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class ProjectServiceTest {

    @MockBean
    ProjectRepository projectRepository;

    private ProjectService projectService;
    private ProjectDto projectDto;

    @Before
    public  void  setup() {
        EntityFactory entityFactory = new EntityFactory();
        projectService = new ProjectService(projectRepository, entityFactory);
    }


    @Test
    public  void  shouldReturnListOfProjects() {

        Project project1 = new Project(1L, "topic1", "PP-1",
                null, "description1");
        Project project2 = new Project(2L, "topic2", "PB-2",
                null, "description1");
        Mockito.when(projectRepository.findAll())
                .thenReturn(List.of(project1, project2));

        List<Project> all = projectService.findAll();
        Assertions.assertThat(all)
                .hasSize(2)
                .contains(project1)
                .contains(project2);
    }
    @Test
    public  void  shouldSaveFromDto() {
        projectDto = new ProjectDto("topic", "PP", LocalDate.of(2021,2,12),
                "leader 1", "description");

        Mockito.when(projectRepository.save(any(Project.class)))
                .thenAnswer((Answer<Project>) invocationOnMock -> {
                    Project project1 = invocationOnMock.getArgument(0);
                    project1.setId(1L);
                    return  project1;
                });

        Project saved = projectService.save(projectDto);

        Assertions.assertThat(saved.getNumber())
                .isEqualTo("PP-1");

    }

}
