package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.commons.HelpfulService;
import pl.pp.back.component.users.RoleTypes;

@RunWith(SpringRunner.class)
public class HelpfulServiceTest {

    private HelpfulService helpfulService = new HelpfulService();


    @Test
    public void shouldFindAllValuesOfRoles() {
        RoleTypes[] roles = helpfulService.getRoles();
        Assertions.assertThat(roles)
                .hasSize(RoleTypes.values().length)
                .contains(RoleTypes.ASSISTANT)
                .contains(RoleTypes.SENIOR_DESIGNER);
    }


}
