package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.notes.*;
import pl.pp.back.component.commons.EntityFactory;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class NoteServiceTest {

    @MockBean
    private NoteRepository noteRepository;

    private NoteService noteService;
    private NoteBuilder noteBuilder;

    private List<Note> notes;
    private Note note1;
    private Note note2;

    @Before
    public void seup() {
        noteService = new NoteService(noteRepository, new EntityFactory());
        noteBuilder = new NoteBuilder();
        NoteDto noteDto1 = new NoteDto("topic1", "very long content1");
        NoteDto noteDto2 = new NoteDto("topic2", "very long content2");
        note1 = noteBuilder.buildEntityFromDto(noteDto1);
        note2 = noteBuilder.buildEntityFromDto(noteDto2);
        notes = List.of(note1, note2);

    }

    @Test
    public void shouldReturnList() {
        Mockito.when(noteRepository.findAll())
                .thenReturn(notes);
        List<Note> noteServiceAll = noteService.findAll();

        Assertions.assertThat(noteServiceAll)
                .isNotEmpty()
                .isEqualTo(notes);
    }

    @Test
    public void shouldFindById() {
        Mockito.when(noteRepository.findById(1L))
                .thenReturn(Optional.of(note1));
        Note noteByService = noteService.findById(1L);

        Assertions.assertThat(noteByService)
                .isEqualTo(note1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowException() {
        Mockito.when(noteRepository.findById(1L))
                .thenReturn(Optional.empty());
        Note noteByService = noteService.findById(1L);

        Assertions.assertThat(noteByService)
                .isNull();
    }

    @Test
    public void shouldSaveNote() {
        NoteDto noteDto3 = new NoteDto("topic3", "very new content");
        Long id = 3L;
        Note note3 = noteBuilder.buildEntityFromDto(noteDto3);
        Mockito.when(noteRepository.save(note3))
                .then((Answer<Note>) answer -> {
                    note3.setId(id);
                    return note3;

                });
        Note save = noteService.save(note3);

        Assertions.assertThat(save.getId())
                .isNotNull()
                .isEqualTo(id);
    }

    @Test
    public void shouldSaveFromDto() {
        NoteDto noteDto3 = new NoteDto("topic3", "very new content");
        Long id = 3L;
        Note note3 = noteBuilder.buildEntityFromDto(noteDto3);
        Mockito.when(noteRepository.save(note3))
                .then((Answer<Note>) answer -> {
                    note3.setId(id);
                    return note3;

                });
        Note save = noteService.save(note3);

        Assertions.assertThat(save.getId())
                .isNotNull()
                .isEqualTo(id);


    }
}
