package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.messages.*;
import pl.pp.back.component.commons.EntityFactory;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
public class MessageServiceTest {

    private MessageService messageService;

    @MockBean
    private MessageRepository messageRepository;

    private Message message1;
    private Message message2;
    List<Message> messages;
    MessageBuilder messageBuilder;

    @Before
    public void setUup() {
        messageBuilder = new MessageBuilder();
        messageService = new MessageService(messageRepository, new EntityFactory());
        MessageDto messageDto1 = new MessageDto("topic1", "content1", "sender1", "receiver1");
        MessageDto messageDto2 = new MessageDto("topic", "content2", "sender2", "receiver2");
        message1 = messageBuilder.buildEntityFromDto(messageDto1);
        message2 = messageBuilder.buildEntityFromDto(messageDto2);
        message1.setId(1L);
        message2.setId(2L);
        messages =  List.of(message1, message2);
    }

        @BeforeEach
                public  void prepareData() {
            MessageDto messageDto1 = new MessageDto("topic1", "content1", "sender1", "receiver1");
            MessageDto messageDto2 = new MessageDto("topic", "content2", "sender2", "receiver2");
            message1 = messageBuilder.buildEntityFromDto(messageDto1);
            message2 = messageBuilder.buildEntityFromDto(messageDto2);
            message1.setId(1L);
            message2.setId(2L);
            messages = List.of(message1, message2);
        }



    @Test
    public void shouldGetListOfMessages() {
        Mockito.when(messageRepository.findAll())
                        .thenReturn(messages);

        List<Message> messageList = messageService.findAll();
        Assertions.assertThat(messageList)
                .contains(message1)
                .contains(message2)
                .hasSize(2);
    }
    @Test
    public  void shouldFindUnreadMessage() {
        Mockito.when(messageRepository.getNumberOfUnReadMessageByReceiver("receiver1"))
                .thenReturn(messages.stream()
                .filter(message -> message.getReceiver().equals("receiver1"))
                        .filter(message -> !message.isRead())
                .count());
        Long numberOfUnread = messageService.countUnreadMessageByReceiver("receiver1");
        Assertions.assertThat(numberOfUnread)
                .isEqualTo(1);

    }
    @Test
    public  void shouldFindMessageById() {
        Mockito.when(messageRepository.findById(1L))
                .thenReturn(messages.stream()
                .filter(message -> message.getId().equals(1L))
                .findAny());

        Message byId = messageService.findById(1L);

        Assertions.assertThat(byId)
                .isNotNull()
                .isEqualTo(message1);
    }
    @Test(expected = EntityNotFoundException.class)
    public  void  shouldThrowException() {

        Mockito.when(messageRepository.findById(15L))
                .thenReturn(Optional.empty());

        Message message = messageService.findById(15L);

        Assertions.assertThat(message)
                .isNull();

    }
    @Test
    public  void shouldSaveMessage() {

        MessageDto messageDto = new MessageDto(
                "t", "c", "s", "r");
        Message saved = messageService.save(messageDto);
        Mockito.verify(messageRepository).save(messageBuilder.buildEntityFromDto(messageDto));
    }
    @Test
    public  void shouldFindMessageByReceiver() {
        Mockito.when(messageRepository.getAllByReceiver("receiver1"))
                .thenReturn(messages
                .stream()
                .filter(message -> message.getReceiver().equals("receiver1"))
                .collect(Collectors.toList()));

        List<Message> listByReceiver = messageService.findAllByReceiver("receiver1");

        Assertions.assertThat(listByReceiver)
                .contains(message1)
                .hasSize(1);

    }

    @Test
    public  void shouldFindMessageBySender() {
        Mockito.when(messageRepository.getAllBySender("sender2"))
                .thenReturn(messages
                        .stream()
                        .filter(message -> message.getSender().equals("sender2"))
                        .collect(Collectors.toList()));

        List<Message> listBySender = messageService.findAllBySender("sender2");

        Assertions.assertThat(listBySender)
                .contains(message2)
                .hasSize(1);

    }
    @Test
    public  void shouldSetReadAsTrue() {

        Mockito.when(messageRepository.findById(1L))
                .thenReturn(messages
                .stream()
                        .filter(message -> message.getId().equals(1L))
                        .findFirst());
        Mockito.when(messageRepository.save(message1))
                .thenReturn(message1);

        Message message = messageService.setAsRead(1L);

        Assertions.assertThat(message.isRead()).isTrue();
    }

    @Test
    public  void  shouldSetFalseForReceiverShow() {
        Long id = message1.getId();
        List<Long> ids = List.of(id) ;
        Mockito.when((messageRepository.setDeleteForReceiver(ids)))
                .then((Answer<Void>) answer ->{
                    message1.setReceiverShow(false);
                    return  null;
                });
        Mockito.when(messageRepository.findById(id))
                .thenReturn(Optional.of(message1));

        messageService.deleteForReceiver(ids);
        Message byId = messageService.findById(id);


        Assertions.assertThat(byId.isReceiverShow())
                .isFalse();
    }
    @Test
    public  void  shouldSetFalseForSenderShow() {
        Long id = message1.getId();
        List<Long> ids = List.of(id);
        Mockito.when((messageRepository.setDeleteForSender(ids)))
                .then((Answer<Void>) answer ->{
                    message1.setSenderShow(false);
                    return  null;
                });
         Mockito.when(messageRepository.findById(id))
                 .thenReturn(Optional.of(message1));

         messageService.deleteForSender(ids);
        Message byId = messageService.findById(id);


        Assertions.assertThat(byId.isSenderShow())
                .isFalse();
    }

    @Test
    public  void shouldNotCallRepositoryMethodForReceivers() {
        List<Long> ids = new ArrayList<>();
        messageService.deleteForReceiver(ids);
        Mockito.verify( messageRepository, Mockito.times(0)).setDeleteForReceiver(ids);
    }
    @Test
    public  void shouldNotCallRepositoryMethodForSenders() {
        List<Long> ids = new ArrayList<>();
        messageService.deleteForSender(ids);
        Mockito.verify( messageRepository, Mockito.times(0)).setDeleteForSender(ids);
    }



}
