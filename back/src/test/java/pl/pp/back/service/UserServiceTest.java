package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.users.*;
import pl.pp.back.component.commons.EntityFactory;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.anyString;


@RunWith(SpringRunner.class)
public class UserServiceTest {

    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    private User user1;
    private  User user2;
    List<User> users;

    @Before
    public  void  setup() {
        users = new ArrayList<>();
        userService = new UserService(userRepository, new EntityFactory());
        user1 = new User(1L, "username1", "pass1", RoleTypes.NONE);
        user2 = new User(2L, "username2", "pass2", RoleTypes.NONE);
        users= List.of(user1, user2);
    }
    @Test
    public  void  shouldFindAll() {
        Mockito.when(userRepository.findAll())
                .thenReturn(users);
        List<User> all = userService.findAll();

        Assertions.assertThat(all)
                .isNotNull()
                .hasSize(2);
    }
    @Test
    public void  shouldFindPasswordByUsername() {
        Mockito.when(userRepository.getPasswordByUserName(anyString()))
                .thenAnswer((Answer<Optional<String>>) invocationOnMock -> {
                    String argument = invocationOnMock.getArgument(0);
                    return users.stream()
                            .filter(user -> user.getUsername().equals(argument))
                            .map(user -> user.getPassword())
                            .findAny();
                });
        String passForUser = userService.getPasswordByUsername("username1");

        Assertions.assertThat(passForUser)
                .isNotNull()
                .isEqualTo("pass1");
    }

    @Test(expected = EntityNotFoundException.class)
    public  void shouldThrowExceptionForPassword() {
            Mockito.when(userRepository.getPasswordByUserName("username3"))
                    .thenReturn(Optional.empty());

        String pass3 = userService.getPasswordByUsername("username3");
            Assertions.assertThat(pass3)
                    .isNull();

    }

    @Test
    public  void shouldSaveUser() {
        Mockito.when(userRepository.save(
                new User(null,"user3", "pass3", RoleTypes.NONE)))
                .thenReturn(new User(3L, "user3", "pass3"));

        User saved = userService.save(new UserDto("user3", "pass3"));

        Assertions.assertThat(saved).isNotNull()
                .isEqualTo(new User(3L, "user3", "pass3"));
    }

    @Test
    public  void shouldFindById() {
        Mockito.when(userRepository.findById(1L))
                .thenReturn(users.stream()
                .filter(user -> user.getId().equals(1L))
                        .findAny());
        User byId = userService.findById(1L);

        Assertions.assertThat(byId)
                .isNotNull()
                .isEqualTo(user1);
    }
    @Test(expected = EntityNotFoundException.class)
    public  void  shouldThrowException() {
        Mockito.when(userRepository.findById(3L))
                .thenReturn(Optional.empty());

        User byId = userService.findById(3L);
        Assertions.assertThat(byId)
                .isNull();

    }
    @Test
    public  void  shouldGetUsernames() {
        Mockito.when(userRepository.findUsernames())
                .thenReturn(users.stream()
                .map(user -> user.getUsername())
                .collect(Collectors.toList()));

        List<String> allUsernames = userService.getAllUsernames();

        Assertions.assertThat(allUsernames)
                .isNotEmpty()
                .hasSize(2)
                .contains("username1");
    }

}
