package pl.pp.back.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.links.*;
import pl.pp.back.component.commons.EntityFactory;

import javax.persistence.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class LinkServiceTest {

    @MockBean
    private LinkRepository linkRepository;

    private LinkService linkService;
    private LinkBuilder linkBuilder;
    private List<Link> links;
    private Link link1;
    private Link link2;

    @Before
    public void setup() {
        linkService = new LinkService(linkRepository, new EntityFactory());
        linkBuilder = new LinkBuilder();
        LinkDto linkDto1 = new LinkDto("name1", "www.pp.pl");
        LinkDto linkDto2 = new LinkDto("name2", "www.pp.pl");

        link1 = linkBuilder.buildEntityFromDto(linkDto1);
        link1.setId(1L);
        link2 = linkBuilder.buildEntityFromDto(linkDto2);
        link2.setId(2L);

        links = List.of(link1, link2);
    }

    @Test
    public void shouldReturnListOfLinks() {
        Mockito.when(linkRepository.findAll())
                .thenReturn(links);

        List<Link> allByService = linkService.findAll();

        Assertions.assertThat(allByService)
                .isNotEmpty()
                .isEqualTo(links);
    }

    @Test
    public  void shouldFindById(){
        Mockito.when(linkRepository.findById(1L))
                .thenReturn(Optional.of(link1));

        Link byId = linkService.findById(1L);

        Assertions.assertThat(byId)
                .isEqualTo(link1);
    }
    @Test(expected = EntityNotFoundException.class)
    public  void shouldThrowENFException () {
        Mockito.when(linkRepository.findById(1L))
                .thenReturn(Optional.empty());

        Link byId = linkService.findById(1L);

        Assertions.assertThat(byId)
                .isNull();
    }

    @Test
    public  void shouldSetIdAfterSave() {

        LinkDto linkDto3 = new LinkDto("name3", "link3");
        Link link3 = linkBuilder.buildEntityFromDto(linkDto3);

        Mockito.when(linkRepository.save(link3))
                .then((Answer<Link>) answer -> {
                            link3.setId(3L);
                            return link3;
                });
                    Link saved = linkService.save(link3);
                    Assertions.assertThat(saved.getId())
                            .isEqualTo(3L);

    }
    @Test
    public  void shouldSaveFromDto() {

        LinkDto linkDto3 = new LinkDto("name3", "link3");

Link link3= linkBuilder.buildEntityFromDto(linkDto3);
        Mockito.when(linkRepository.save(link3))
                .then((Answer<Link>) answer -> {
                    link3.setId(3L);
                    return link3;
                });
        Link saved = linkService.create(linkDto3);
        Assertions.assertThat(saved.getName())
                .isEqualTo("name3");

    }



}
