package pl.pp.back.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.messages.MessageDto;
import pl.pp.back.component.messages.Message;
import pl.pp.back.component.messages.MessageBuilder;
import pl.pp.back.component.messages.MessageRepository;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MessageRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MessageRepository messageRepository;
    private Message message;

    private  MessageBuilder messageBuilder = new MessageBuilder();


    @BeforeEach
    public void clear() {
        entityManager.clear();



    }

    @Test
    public void shouldReturnMessageById() {

        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);
        entityManager.persist(message);
        entityManager.flush();
        Message found = messageRepository.findById(message.getId()).get();
        Assertions.assertThat(found).isEqualTo(message);
    }

    @Test
    public void shouldFindByReceiver() {

        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);
        entityManager.persist(message);
        entityManager.flush();
        List<Message> messagesByReceiver = messageRepository.getAllByReceiver("receiver");
        Assertions.assertThat(messagesByReceiver).contains(message)
                .hasSameSizeAs(new int[1]);
    }

    @Test
    public void shouldFindBySender() {

        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);
        entityManager.persist(message);
        entityManager.flush();
        List<Message> messagesBySender = messageRepository.getAllBySender("sender");
        Assertions.assertThat(messagesBySender).contains(message)
                .hasSameSizeAs(new int[1]);
    }

    @Test
    public void shouldFindNumberOfUnreadMessage() {

        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);
        entityManager.persist(message);
        entityManager.flush();
        Long unread = messageRepository.getNumberOfUnReadMessageByReceiver("receiver");
        Assertions.assertThat(unread)
                .isNotNull()
                .isEqualTo(1);
    }

    @Test
    public void shouldSetFalseForSender() {
        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);

        message = messageRepository.save(message);
        messageRepository.flush();

entityManager.clear();
        long id = message.getId();
        entityManager.flush();
//

        messageRepository.setDeleteForSender(List.of(message.getId()));


        Message message1 = messageRepository.findById(id).get();
        System.out.println(message1);

        Assertions.assertThat(message1.isSenderShow()).isFalse();
    }

    @Test
    public void shouldSetFalseForReceiver() {
        MessageDto messageDto =
                new MessageDto("topic", "content", "sender", "receiver");
        message = messageBuilder.buildEntityFromDto(messageDto);

        message = messageRepository.save(message);
        messageRepository.flush();

        entityManager.clear();
        long id = message.getId();
        entityManager.flush();
//

        messageRepository.setDeleteForReceiver(List.of(message.getId()));


        Message message1 = messageRepository.findById(id).get();
        System.out.println(message1);

        Assertions.assertThat(message1.isReceiverShow()).isFalse();
    }

}
