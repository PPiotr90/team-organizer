package pl.pp.back.repository;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.notes.NoteBuilder;
import pl.pp.back.component.notes.NoteRepository;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NoteRepositoryTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    NoteRepository noteRepository;

    private Note note1;
    private Note note2;
    private NoteDto noteDto1;
    private NoteDto noteDto2;
    private NoteBuilder noteBuilder;
    @Before
    public  void  setup() {
        noteDto1 = new NoteDto("topic1", "very long content1");
        noteDto2 = new NoteDto("topic2", "very long content2");
        noteBuilder = new NoteBuilder();
        note1 = noteBuilder.buildEntityFromDto(noteDto1);
        note2 = noteBuilder.buildEntityFromDto(noteDto2);
    }

    @Test
public  void  shouldSaveAndSetId() {
        Note saved = noteRepository.save(note1);

        Assertions.assertThat(saved.getId())
                .isNotNull();
    }
    @Test
    public  void  shouldFindLinkById(){
        Note noteByBuilder = noteBuilder.buildEntityFromDto(noteDto1);

        entityManager.persist(noteByBuilder);
        entityManager.flush();

        Note noteById = noteRepository.findById(noteByBuilder.getId()).get();

        Assertions.assertThat(noteById)
                .isEqualTo(noteByBuilder);
    }

    @Test
    public  void  shouldDeleteById() {
        entityManager.persist(note1);
        entityManager.persist(note2);

        entityManager.flush();
        noteRepository.deleteById(note1.getId());

        List<Note> all = noteRepository.findAll();
        Assertions.assertThat(all)
                .doesNotContain(note1)
                .contains(note2);
    }
    @Test
    public  void  shouldDeleteAll() {
        entityManager.persist(note1);
        entityManager.persist(note2);

        entityManager.flush();

        noteRepository.deleteAll();
        List<Note> all = noteRepository.findAll();
        Assertions.assertThat(all)
                .isEmpty();
    }
    @Test
    public  void shouldFindAll() {
        entityManager.persist(note1);
        entityManager.persist(note2);

        entityManager.flush();


        List<Note> all = noteRepository.findAll();
        Assertions.assertThat(all)
                .contains(note1)
                .contains(note2)
                .hasSize(2);
    }
    @Test
    public  void shouldUpdateNote() {
        String  newTopic = "new topic for note";
        Note saved = noteRepository.save(note1);
        saved.setTopic(newTopic);
        noteRepository.save(saved);
        Note read = noteRepository.findById(saved.getId()).get();
        Assertions.assertThat(read.getTopic())
                .isEqualTo(newTopic);
    }
}
