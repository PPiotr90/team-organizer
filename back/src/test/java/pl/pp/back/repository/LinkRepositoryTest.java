package pl.pp.back.repository;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.links.LinkBuilder;
import pl.pp.back.component.links.LinkRepository;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LinkRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private LinkRepository linkRepository;

    private Link link1;
    private Link link2;
    private LinkDto linkDto1;
    private LinkDto linkDto2;
    private LinkBuilder linkBuilder = new LinkBuilder();

    @Before
    public void clear() {

        entityManager.clear();
        linkDto1 = new LinkDto("name1", "www.pp.pl");
        linkDto2 = new LinkDto("name2", "www.pp.pl");

        link1 = linkBuilder.buildEntityFromDto(linkDto1);
        link2 = linkBuilder.buildEntityFromDto(linkDto2);
    }
    @Test
    public  void shouldSaveAndSetId() {
        Link saved = linkRepository.save(link1);

        Assertions.assertThat(saved.getId())
                .isNotNull();
    }

    @Test
    public void shouldFindLinkById() {


        Link linkByBuilder = linkBuilder.buildEntityFromDto(linkDto1);

        entityManager.persist(linkByBuilder);
        entityManager.flush();

        Link linkByRepository = linkRepository.findById(linkByBuilder.getId()).get();

        Assertions.assertThat(linkByBuilder)
                .isEqualTo(linkByRepository);
    }

    @Test
    public void shouldFindListOfAllElements() {

        entityManager.persist(link1);
        entityManager.persist(link2);
        entityManager.flush();
        List<Link> allByRepository = linkRepository.findAll();
        Assertions.assertThat(allByRepository)
                .isNotEmpty()
                .hasSize(2);
    }

    @Test
    public void shouldDeleteById() {

        entityManager.persist(link1);
        entityManager.persist(link2);
        entityManager.flush();
        linkRepository.deleteById(link1.getId());
        List<Link> allByRepository = linkRepository.findAll();
        Assertions.assertThat(allByRepository)
                .isNotEmpty()
                .hasSize(1)
                .doesNotContain(link1);
    }

    @Test
    public void shouldDeleteAll() {

        entityManager.persist(link1);
        entityManager.persist(link2);
        entityManager.flush();
        linkRepository.deleteAll();
        List<Link> allByRepository = linkRepository.findAll();
        Assertions.assertThat(allByRepository)
                .isEmpty();
    }

    @Test
    public  void  shouldFindAll() {
        entityManager.persist(link1);
        entityManager.persist(link2);
        entityManager.flush();

        List<Link> allByRepository = linkRepository.findAll();
        Assertions.assertThat(allByRepository)
                .contains(link1)
                .contains(link2)
                .hasSize(2);
    }

    @Test
    public  void  shouldUpdate() {
        String newName = "new name";
        Link save = linkRepository.save(link1);
        save.setName(newName);
        linkRepository.save(save);

        Link read = linkRepository.findById(save.getId()).get();
        Assertions.assertThat(read.getName())
                .isEqualTo(newName);


    }
}

