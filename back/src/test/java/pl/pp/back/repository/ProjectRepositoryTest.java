package pl.pp.back.repository;


import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.projects.ProjectRepository;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ProjectRepository projectRepository;
    private Project project;


    @BeforeEach
    public void clear() {
        entityManager.clear();



    }

    @Test
    public void shouldFindById() {
        project = new Project(null, "topic", "PP-1", LocalDate.of(2021,1,1)
                , "description");

        entityManager.persist(project);
        entityManager.flush();

        Project byId = projectRepository.findById(project.getId()).get();
        Assertions.assertThat(byId).isEqualTo(project);

    }

    @Test
    public void shouldFindAllWithProject() {
        project = new Project(null, "topic", "PP-1", LocalDate.of(2021,1,1)
                , "description");

        entityManager.persist(project);
        entityManager.flush();
        List<Project> all = projectRepository.findAll();

        Assertions.assertThat(all)
                .contains(project);

    }
@Test
    public  void  shouldDeleteById() {
        project = new Project(null, "topic", "PP-1", LocalDate.of(2021,1,1)
                , "description");

        entityManager.persist(project);
        entityManager.flush();
        projectRepository.deleteById(project.getId());

        Optional<Project> byId = projectRepository.findById(project.getId());

        Assertions.assertThat(byId).isEmpty();


    }

}
