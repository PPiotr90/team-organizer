package pl.pp.back.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.pp.back.component.users.User;
import pl.pp.back.component.users.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    private User user;

    @BeforeEach
    public void clear() {
        entityManager.clear();

    }

    @Test
    public void checkUsernameIsInBase() {
        user = new User("username", "pass");
        entityManager.persist(user);
        entityManager.flush();
        List<String> usernames = userRepository.findUsernames();
        Assertions.assertThat(usernames).contains(user.getUsername());
    }

    @Test
    public void shouldGetEmptyOptional() {
        user = new User("username", "pass");
        entityManager.persist(user);
        entityManager.flush();
        Assertions.assertThat(userRepository.findByUsername("user0"))
                .isEmpty();
    }

    @Test
    public void shouldFindUser() {
        user = new User("username", "pass");
        entityManager.persist(user);
        entityManager.flush();
        User findUser = userRepository.findByUsername("username").get();
        Assertions.assertThat(findUser)
                .isEqualTo(user);
    }

    @Test
    public void shouldFindUsersPassword() {
        user = new User("username", "pass");
        entityManager.persist(user);
     String passwordByUsername = userRepository.getPasswordByUserName("username").get();
        Assertions.assertThat(passwordByUsername)
                .isNotNull()
                .isEqualTo("pass");
    }


}
