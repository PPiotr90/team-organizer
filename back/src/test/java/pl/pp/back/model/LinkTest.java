package pl.pp.back.model;


import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.pp.back.component.links.Link;

public class LinkTest {

    Link link;

    @Before
    public  void  setup() {
        link = new Link();
    }
    @Test
    public void shouldSetId() {

        long id = 15L;
        link.setId(id);

        Assertions.assertThat(link.getId())
                .isEqualTo(id);
    }

    @Test
    public  void  shouldSetName() {
        String name = "name";
        link.setName(name);

        Assertions.assertThat(link.getName())
                .isEqualTo(name);

    }

    @Test
    public  void  shouldSetContent() {
        String content = " www.google.pl";
       link.setContent(content);

        Assertions.assertThat(link.getContent())
                .isEqualTo(content);
    }


}
