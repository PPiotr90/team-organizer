package pl.pp.back.model;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pp.back.component.registries.ActionTypes;
import pl.pp.back.component.registries.Registry;
import pl.pp.back.component.users.User;

import java.time.LocalDate;


class RegistryTest {

    private Registry registry;

    @BeforeEach
    public void setRegistry() {
        this.registry = new Registry();
    }

    @Test
    public void shouldSetId() {
        registry.setId(5L);
        Assertions.assertThat(registry.getId())
                .isNotNull()
                .isEqualTo(5);
    }

    @Test
    public void shouldSetHours() {
        registry.setHours(8);
        Assertions.assertThat(registry.getHours())
                .isEqualTo(8);
    }

    @Test
    public void shouldSetDate() {
        LocalDate today = LocalDate.of(2020, 12, 10);

        registry.setDate(today);

        Assertions.assertThat(registry.getDate())
                .isEqualTo(today);
    }
    @Test
    public  void  shouldSetAction() {
        registry.setAction(ActionTypes.MAKING_DRAWINGS);
        Assertions.assertThat(registry.getAction())
                .isNotNull()
                .isEqualTo(ActionTypes.MAKING_DRAWINGS);
    }
    @Test
    public  void shouldSetUser() {
        User user = new User("pp", "pass");
        registry.setUser(user);
        Assertions.assertThat(registry.getUser())
                .isEqualTo(user);

    }
}
