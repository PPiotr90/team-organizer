package pl.pp.back.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pp.back.component.projects.Project;

import java.time.LocalDate;

class ProjectTest {
    private Project project;

    @BeforeEach
    public  void  setProject() {
        project = new Project();
    }
    @Test
    public void shouldSetId() {
        project.setId(17l);
        Assertions.assertThat(project.getId())
                .isNotNull()
                .isEqualTo(17);
    }
    @Test
    public void shouldSetNumber() {
       project.setNumber("RZ-147");
        Assertions.assertThat(project.getNumber())
                .isNotNull()
                .isEqualTo("RZ-147");
    }
    @Test
    public void shouldSetFinishDate() {
        LocalDate finishDate = LocalDate.of(1998,12,17);
        project.setFinishDate(finishDate);
        Assertions.assertThat(project.getFinishDate())
                .isNotNull()
                .isEqualTo(finishDate);
    }
    @Test
    public void shouldSetTopic() {
        project.setTopic("new topic");
        Assertions.assertThat(project.getTopic())
                .isNotNull()
                .isEqualTo("new topic");
    }
    @Test
    public void shouldSetDescription() {
        project.setDescription("my own description");
        Assertions.assertThat(project.getDescription())
                .isNotNull()
                .isEqualTo("my own description");
    }
}
