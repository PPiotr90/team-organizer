package pl.pp.back.model;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pp.back.component.users.User;


class UserTest {

    private User user;

    @BeforeEach
    public  void  setUser() {
        user = new User();
    }
    @Test
    public void shouldSetId() {
        user.setId(1l);
        Assertions.assertThat(user.getId())
                .isEqualTo(1l);
    }
    @Test
    public void shouldSetUsername() {
        user.setUsername("user0");
        Assertions.assertThat(user.getUsername())
                .isNotNull()
                .isEqualTo("user0");
    }
    @Test
    public void shouldSetPassword() {
        user.setPassword("pass1");
        Assertions.assertThat(new String(user.getPassword()))
                .isNotNull()
                .isEqualTo("pass1");
    }
}
