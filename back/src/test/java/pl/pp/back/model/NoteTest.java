package pl.pp.back.model;


import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.pp.back.component.notes.Note;


public class NoteTest {

    Note note;

    @Before
    public void setup() {
        note = new Note();
    }

    @Test
    public void shouldSetId() {

        long id = 15L;
        note.setId(id);

        Assertions.assertThat(note.getId())
                .isEqualTo(id);
    }

    @Test
    public  void  shouldSetTopic() {
        String topic = "topic1";
        note.setTopic(topic);

        Assertions.assertThat(note.getTopic())
                .isEqualTo(topic);

    }

    @Test
    public  void  shouldSetContent() {
        String content = " new content";
        note.setContent(content);

        Assertions.assertThat(note.getContent())
                .isEqualTo(content);
    }

}
