package pl.pp.back.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pp.back.component.messages.Message;


class MessageTest {

    private Message message;
    @BeforeEach
    public   void setNewMessage() {
        this.message = new Message();
    }
    @Test
    public  void shouldSetId() {
        this.message.setId(150L);
        Assertions.assertThat(message.getId()).isEqualTo(150);
    }

    @Test
    public  void  shouldSetTopic() {
        message.setTopic("new topic");
        Assertions.assertThat(message.getTopic())
                .isNotNull()
                .isEqualTo("new topic");
    }

    @Test
    public  void  shouldSetContent() {
        message.setContent("new content");
        Assertions.assertThat(message.getContent())
                .isNotNull()
                .isEqualTo("new content");
    }
    @Test
    public  void  shouldSetSender() {
        message.setSender("sender");
        Assertions.assertThat(message.getSender())
                .isNotNull()
                .isEqualTo("sender");
    }
    @Test
    public  void  shouldSetReceiver() {
        message.setReceiver("receiver");
        Assertions.assertThat(message.getReceiver())
                .isNotNull()
                .isEqualTo("receiver");
    }
    @Test
    public  void  setMessageAsRead() {
        message.setRead(true);
        Assertions.assertThat(message.isRead())
                .isTrue();
    }
}
