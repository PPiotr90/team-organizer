package pl.pp.back.controller;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.pp.back.component.links.LinkController;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.links.LinkBuilder;
import pl.pp.back.component.links.LinkService;


import java.util.HashMap;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LinkController.class)
public class LinkControllerTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    LinkService linkService;

    private final String url = "/links";

    private JsonMapper jsonMapper;
    private LinkDto linkDto1;
    private LinkDto linkDto2;
    private Link link1;
    private Link link2;
    private List<Link> links;
    private LinkBuilder linkBuilder;

    @Before
    public void setup() {
        jsonMapper = new JsonMapper();
        linkBuilder = new LinkBuilder();

        linkDto1 = new LinkDto("name1", "www.pp.pl");
        linkDto2 = new LinkDto("name2", "www.pp.pl");

        link1 = linkBuilder.buildEntityFromDto(linkDto1);
        link2 = linkBuilder.buildEntityFromDto(linkDto2);
        links = List.of(link1, link2);
    }

    @Test
    public void shouldReturnList() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON);

        BDDMockito.given(linkService.findAll())
                .willReturn(links);

        String content = mvc.perform(builder).
                andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Link> linkList = jsonMapper.readValue(content, List.class);
        Assertions.assertThat(linkList).hasSize(2);
    }

    @Test
    public void shouldFindById() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.get(url + "/1")
                .contentType(MediaType.APPLICATION_JSON);
        BDDMockito.given(linkService.findById(1L))
                .willReturn(link1);

        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Link linkFromRequest = jsonMapper.readValue(content, Link.class);

        Assertions.assertThat(linkFromRequest)
                .isEqualTo(link1);
    }

    @Test
    public void shouldDeleteById() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.delete(url + "/1");


        mvc.perform(builder).
                andExpect(MockMvcResultMatchers.status().isAccepted());

    }

    @Test
    public void shouldSaveFromDto() throws Exception {

        Mockito.when(linkService.create(linkDto1))
                .then((Answer<Link>) answer -> {
                    link1.setId(1L);
                    return link1;
                });

        String bodyToPost = jsonMapper.writeValueAsString(linkDto1);

        MockHttpServletRequestBuilder post = MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyToPost);

        String contentAfterPost = mvc.perform(post)
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Link linkFromSave = jsonMapper.readValue(contentAfterPost, Link.class);

        Assertions.assertThat(linkFromSave)
                .isEqualTo(link1);
    }

    @Test
    public void shouldUpdate() throws Exception {
        String newName = "new name";
        HashMap<Long, Link> linkHashMap = new HashMap<>();
        Link link3 = linkBuilder.
                buildEntityFromDto(new LinkDto("link3", "content3"));
        link3.setId(3L);
        Mockito.when(linkService.save(link3))
                .then((Answer<Link>) answer -> {
                    Link link = answer.getArgument(00);
                    Link linkToMap = new
                            Link(link.getId(), link.getName(), link.getContent());
                    linkHashMap.put(linkToMap.getId(), linkToMap);
                    return linkToMap;
                });
        link3.setName(newName);
        String body = jsonMapper.writeValueAsString(link3);
        MockHttpServletRequestBuilder put = MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body);

        mvc.perform(put);


        Assertions.assertThat(linkHashMap.get(3L).getName())
                .isEqualTo(newName);


    }


}
