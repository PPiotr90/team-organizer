package pl.pp.back.controller;


import com.fasterxml.jackson.databind.json.JsonMapper;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import pl.pp.back.component.notes.NoteController;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.notes.Note;

import pl.pp.back.component.notes.NoteBuilder;
import pl.pp.back.component.notes.NoteService;

import java.util.HashMap;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(NoteController.class)
public class NoteControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    NoteService noteService;


    private final String url = "/notes";

    private JsonMapper jsonMapper;
    private NoteDto noteDto1;
    private NoteDto NoteDto2;
    private Note note1;
    private Note note2;
    private List<Note> notes;
    private NoteBuilder noteBuilder;

    @Before
    public void setup() {
        jsonMapper = new JsonMapper();
        noteBuilder = new NoteBuilder();

        noteDto1 = new NoteDto("topic1", "content1");
        NoteDto2 = new NoteDto("topic2", "Content2");

        note1 = noteBuilder.buildEntityFromDto(noteDto1);
        note2 = noteBuilder.buildEntityFromDto(NoteDto2);
        notes = List.of(note1, note2);
    }

    @Test
    public void shouldReturnList() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON);

        BDDMockito.given(noteService.findAll())
                .willReturn(notes);

        String content = mvc.perform(builder).
                andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Note> noteList = jsonMapper.readValue(content, List.class);
        Assertions.assertThat(noteList).hasSize(2);
    }

    @Test
    public void shouldFindById() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.get(url + "/1")
                .contentType(MediaType.APPLICATION_JSON);
        BDDMockito.given(noteService.findById(1L))
                .willReturn(note1);

        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Note noteFromRequest = jsonMapper.readValue(content, Note.class);

        Assertions.assertThat(noteFromRequest)
                .isEqualTo(note1);
    }

    @Test
    public void shouldDeleteById() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.delete(url + "/1");

        mvc.perform(builder).
                andExpect(MockMvcResultMatchers.status().isAccepted());

    }

    @Test
    public void shouldSaveFromDto() throws Exception {

        Mockito.when(noteService.create(noteDto1))
                .then((Answer<Note>) answer -> {
                    note1.setId(1L);
                    return note1;
                });

        String bodyToPost = jsonMapper.writeValueAsString(noteDto1);
        System.out.println(bodyToPost);
        MockHttpServletRequestBuilder post = MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyToPost);

        String contentAfterPost = mvc.perform(post)
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();
        System.out.println(contentAfterPost);
        Note noteFromSave = jsonMapper.readValue(contentAfterPost, Note.class);

        Assertions.assertThat(noteFromSave)
                .isEqualTo(note1);
    }

    @Test
    public void shouldUpdate() throws Exception {
        String newTopic = "new topic";
        HashMap<Long, Note> noteHashMap = new HashMap<>();
        Note note3 = noteBuilder.
                buildEntityFromDto(new NoteDto("topic3", "content3"));
        note3.setId(3L);
        Mockito.when(noteService.save(note3))
                .then((Answer<Note>) answer -> {
                    Note note = answer.getArgument(00);
                    Note noteToMap = new
                            Note(note.getId(), note.getTopic(), note.getContent());
                    noteHashMap.put(noteToMap.getId(), noteToMap);
                    return noteToMap;
                });
        note3.setTopic(newTopic);
        String body = jsonMapper.writeValueAsString(note3);
        MockHttpServletRequestBuilder put = MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body);

        mvc.perform(put);


        Assertions.assertThat(noteHashMap.get(3L).getTopic())
                .isEqualTo(newTopic);


    }


}
