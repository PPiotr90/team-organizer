package pl.pp.back.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.pp.back.component.commons.HelpfullController;
import pl.pp.back.component.users.RoleTypes;
import pl.pp.back.component.commons.HelpfulService;

import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
@WebMvcTest(HelpfullController.class)
public class HelpfulControllerTest {

    @Autowired
    private MockMvc mvc;


    @MockBean
    private HelpfulService helpfulService;

    @Test
    public void shouldReturnListOfRoles() throws Exception {

        given(helpfulService.getRoles())
                .willReturn(RoleTypes.values());

        RequestBuilder builder = MockMvcRequestBuilders
                .get("/roles")
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }





}
