package pl.pp.back.controller;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.pp.back.component.users.UserController;
import pl.pp.back.component.users.UserDto;
import pl.pp.back.component.users.RoleTypes;
import pl.pp.back.component.users.User;
import pl.pp.back.component.users.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;



@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private UserService userService;


    @Autowired
    private MockMvc mvc;
    private final String url = "/users";
    private JsonMapper jsonMapper;
    private UserDto userDto1;
    private UserDto userDto2;
    private User user1;
    private User user2;
    private List<User> users;

    @Before
    public void setup() {
        jsonMapper = new JsonMapper();
        userDto1 = new UserDto("login", "pass");
        userDto2 = new UserDto("login2", "pass2");
        users = new ArrayList<>();

    }

    @Test
    public void shouldCallGetAll() throws Exception {
        given(userService.findAll())
                .willReturn(users);

        RequestBuilder builder = MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON);

        String contentAsString = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List list = jsonMapper.readValue(contentAsString, List.class);

        Assertions.assertThat(list)
                .isEqualTo(users)
                .isEmpty();
    }

    @Test
    public void shouldSaveUserFromDto() throws Exception {
       final Long id = 5L;
        given(userService.save(userDto1))
                .will((Answer<User>) answer -> {
                    UserDto userDto = answer.getArgument(0);
                    User result =  new User(id, userDto.getUsername(),
                            userDto.getPassword(), RoleTypes.NONE);
                    return result;
                });

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonMapper.writeValueAsString(userDto1));

        String contentAsString = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        User savedUser = jsonMapper.readValue(contentAsString, User.class);
        Assertions.assertThat(savedUser.getId())
                .isNotNull()
                .isEqualTo(id);


    }

    @Test
    public void shouldFindById() throws Exception {
        given(userService.findById(2L))
                .willReturn(new User(2L, "user2", "pass2", RoleTypes.PRESIDENT));


        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(url + "/2")
                .contentType(MediaType.APPLICATION_JSON);

        String contentAsString = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        User savedUser = jsonMapper.readValue(contentAsString, User.class);
        Assertions.assertThat(savedUser.getRole())
                .isEqualTo(RoleTypes.PRESIDENT);

    }

    @Test
    public void shouldThrowException() throws Exception {
        given(userService.getPasswordByUsername("login1"))
                .willReturn("pass1");


        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "/pass/login1")
                .contentType(MediaType.APPLICATION_JSON);

        String contentAsString = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();


        Assertions.assertThat(contentAsString)
                .isEqualTo("pass1")
                .isNotNull();

    }

    @Test
    public void shouldReturnUsernamesList() throws Exception {
        given(userService.getAllUsernames())
                .willReturn(List.of("user1", "user2"));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(url + "/usernames")
                .contentType(MediaType.APPLICATION_JSON);

        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List list = jsonMapper.readValue(content, List.class);

        Assertions.assertThat(list)
                .hasSize(2)
                .contains("user1");
    }

}

