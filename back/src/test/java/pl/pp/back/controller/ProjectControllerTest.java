package pl.pp.back.controller;


import com.fasterxml.jackson.databind.json.JsonMapper;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.pp.back.component.projects.ProjectController;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.projects.ProjectService;

import java.util.List;

import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
@WebMvcTest(ProjectController.class)
public class ProjectControllerTest {

    @MockBean
    private ProjectService projectService;
    private JsonMapper jsonMapper;

    @Before
    public void setup() {
        jsonMapper = new JsonMapper();
    }


    @Autowired
    private MockMvc mvc;
    private final String url = "/projects";

    @Test
    public void shouldReturnListOfProjects() throws Exception {
        Project project1 = new Project(1L, "topic1", "PP-1",
                null, "description1");
        Project project2 = new Project(2L, "topic2", "PB-2",
                null, "description1");
        given(projectService.findAll())
                .willReturn(List.of(project1, project2));

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON);
        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<Project> list = jsonMapper.readValue(content, List.class);



        Assertions.assertThat(list)
                .hasSize(2).hasSize(2);
    }

}
