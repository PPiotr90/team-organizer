package pl.pp.back.controller;


import com.fasterxml.jackson.databind.json.JsonMapper;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.pp.back.component.messages.MessageController;
import pl.pp.back.component.messages.MessageDto;
import pl.pp.back.component.messages.Message;
import pl.pp.back.component.messages.MessageBuilder;
import pl.pp.back.component.messages.MessageService;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(MessageController.class)
public class MessageControllerTest {

    @MockBean
    private MessageService messageService;


    @Autowired
    private MockMvc mvc;


    private final String url = "/messages";
    private JsonMapper jsonMapper;
    private MessageDto messageDto1;
    private MessageDto messageDto2;
    private Message message1;
    private Message message2;
    private List<Message> messages;
    private  MessageBuilder messageBuilder;

    @Before
    public void setup() {
        jsonMapper = new JsonMapper();
        messageDto1 = new MessageDto("topic1", "content1",
                "sender1", "receiver1");
        messageDto2 = new MessageDto("topic2", "content2",
                "sender2", "receiver2");
        messageBuilder = new MessageBuilder();
        message1 = messageBuilder.buildEntityFromDto(messageDto1);
        message1.setSendTime(null);
        message2 = messageBuilder.buildEntityFromDto(messageDto2);
        message2.setSendTime(null);
        messages = List.of(message1, message2);
    }

    @Test
    public void shouldGetAll() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON);

        given(messageService.findAll())
                .willReturn(messages);
        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Message> messageList = jsonMapper.readValue(content, List.class);
        Assertions.assertThat(messageList).hasSize(2);

    }

    @Test
    public void shouldFindById() throws Exception {

        given(messageService.findById(1L))
                .willReturn(message1);
        RequestBuilder builder = MockMvcRequestBuilders.get(url + "/1")
                .contentType(MediaType.APPLICATION_JSON);

        String content = mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Message message = jsonMapper.readValue(content, Message.class);

        Assertions.assertThat(message).isEqualTo(message1);


    }


    @Test
    public void shouldFindBySender() throws Exception {
        given(messageService.findAllBySender("sender1"))
                .willReturn(List.of(message1));
        RequestBuilder builder = MockMvcRequestBuilders.get(url + "/sender/sender1")
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn()
                .getResponse()
                .getContentAsString();

    }

    @Test
    public void shouldFindByReceiver() throws Exception {
        given(messageService.findAllByReceiver("receiver1"))
                .willReturn(List.of(message1));
        RequestBuilder builder = MockMvcRequestBuilders.get(url + "/receiver/receiver1")
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn();

    }

    @Test
    public void shouldCountUnreadMessage() throws Exception {
        given(messageService.countUnreadMessageByReceiver("receiver1"))
                .willReturn(1L);

        RequestBuilder builder = MockMvcRequestBuilders
                .get(url + "/unread/receiver1")
                .contentType(MediaType.APPLICATION_JSON);

        String content = mvc.perform(builder)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Long unread = Long.parseLong(content);
        Assertions.assertThat(unread)
                .isEqualTo(1L);
    }

    @Test
    public void shouldSetReadAsTrue() throws Exception {
        given(messageService.setAsRead(1L))
                .will((Answer<Message>) answer -> {
                    message1.setRead(true);
                    return message1;
                });
        RequestBuilder requestBuilder= MockMvcRequestBuilders
                .patch(url+"/read/1")
                .contentType(MediaType.APPLICATION_JSON);
        String content = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Message message = jsonMapper.readValue(content, Message.class);

        Assertions.assertThat(message.isRead()).isTrue();


    }

    @Test
    public void shouldSetFalseReceiverShow() throws Exception {
        message1.setId(1L);

        given(messageService.deleteForReceiver(List.of(message1.getId())))
                .will((Answer<Void>) answer -> {
                    message1.setReceiverShow(false);
                    message1.setSendTime(null);
                    return null;
                });
        given(messageService.findById(message1.getId()))
                .willReturn(message1);
        RequestBuilder requestBuilder= MockMvcRequestBuilders
                .patch(url+"/delete/receiver")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonMapper.writeValueAsString(new Long[]{message1.getId()}));



        RequestBuilder requestBuilder1= MockMvcRequestBuilders
                .get(url+"/"+message1.getId())
                .contentType(MediaType.APPLICATION_JSON);


        mvc.perform(requestBuilder);
        String content = mvc.perform(requestBuilder1)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();



        Message message = jsonMapper.readValue(content, Message.class);
        System.out.println(message);
        System.out.println(message.isReceiverShow());
        Assertions.assertThat(message.isReceiverShow())
                .isFalse();


    }
    @Test
    public void shouldSetFalseSenderShow() throws Exception {
        message1.setId(1L);

        given(messageService.deleteForSender(List.of(message1.getId())))
                .will((Answer<Void>) answer -> {
                    message1.setSenderShow(false);
                    message1.setSendTime(null);
                    return null;
                });
        given(messageService.findById(message1.getId()))
                .willReturn(message1);
        RequestBuilder requestBuilder= MockMvcRequestBuilders
                .patch(url+"/delete/sender")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonMapper.writeValueAsString(new Long[]{message1.getId()}));



        RequestBuilder requestBuilder1= MockMvcRequestBuilders
                .get(url+"/"+message1.getId())
                .contentType(MediaType.APPLICATION_JSON);


        mvc.perform(requestBuilder);
        String content = mvc.perform(requestBuilder1)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();



        Message message = jsonMapper.readValue(content, Message.class);

        Assertions.assertThat(message.isSenderShow()).isFalse();


    }

    @Test
    public  void  shouldAddMessage() throws Exception {
        MessageDto messageDto = new MessageDto("topic3", "content3",
                "sender3", "receiver3");

        String body = jsonMapper.writeValueAsString(messageDto);
        given(messageService.save(messageDto))
                .will((Answer<Message>) answer ->{
                    MessageDto messageDtoToSave= answer.getArgument(0);
                    Message messageToSave = messageBuilder
                            .buildEntityFromDto(messageDtoToSave);
                    messageToSave.setId(3L);
                    messageToSave.setSendTime(null);
                    return messageToSave;
                });
        RequestBuilder requestBuilder= MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body);
        String content = mvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();



        Message message = jsonMapper.readValue(content, Message.class);


        Assertions.assertThat(message.getId())
                .isNotNull()
                .isEqualTo(3L);
    }

}
