package pl.pp.back.patterns.builder;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pp.back.component.messages.MessageBuilder;
import pl.pp.back.component.messages.MessageDto;
import pl.pp.back.component.messages.Message;

import java.time.LocalDateTime;


class MessageBuilderTest {
    MessageBuilder messageBuilder;

    @BeforeEach
    public  void  setMessageBuilder() {
        messageBuilder = new MessageBuilder();
    }
    @Test
    public  void shouldSetTopic() {
        String topic ="topic 1";
        Assertions.assertThat(messageBuilder
                .setTopic(topic).build().getTopic())
                .isNotNull()
                .isEqualTo(topic);
    }
    @Test
    public  void shouldSetContent() {
        String content ="content 1";
        Assertions.assertThat(messageBuilder
                .setContent(content).build().getContent())
                .isNotNull()
                .isEqualTo(content);
    }
    @Test
    public  void shouldSetSender() {
        String sender ="sender 001";
        Assertions.assertThat(messageBuilder
                .setSender(sender).build().getSender())
                .isNotNull()
                .isEqualTo(sender);
    }
    @Test
    public  void shouldSetReceiver() {
        String receiver ="topic 1";
        Assertions.assertThat(messageBuilder
                .setReceiver(receiver).build().getReceiver())
                .isNotNull()
                .isEqualTo(receiver);
    }
    @Test
    public  void  newMessageShouldBeNotRead() {
        Assertions.assertThat(messageBuilder.build().isRead())
                .isFalse();
    }
    @Test
    public  void newMessageShouldByShowed() {
        Assertions.assertThat(messageBuilder.build().isReceiverShow())
                .isTrue().
                isEqualTo(messageBuilder.build().isSenderShow());
    }
    @Test
    public  void  shouldBuildMessageFromDto() {
        Long id = null;
        String topic = "topic";
        String content = "content";
        String sender = "sender";
        String receiver = "receiver";
        LocalDateTime send = LocalDateTime.now();
        MessageDto messageDto = new MessageDto(topic,content, sender, receiver);
        Message messageByBuilder = messageBuilder.buildEntityFromDto(messageDto);
        messageByBuilder.setSendTime(send);
        Message expected = new Message( id, topic,content, send, true,
                true, sender, receiver, false);
        Assertions.assertThat(messageByBuilder)
                .isEqualTo(expected);

    }
}
