package pl.pp.back.patterns.builder;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.pp.back.component.users.UserBuilder;
import pl.pp.back.component.users.UserDto;
import pl.pp.back.component.users.RoleTypes;
import pl.pp.back.component.users.User;


public class UserBuilderTest {

    private UserBuilder userBuilder;

    @Before
    public  void  setup() {
        userBuilder = new UserBuilder();
    }
    @Test
    public  void shouldReturnEmptyUser() {
        User builtUser = userBuilder.build();
        Assertions.assertThat(builtUser)
                .isEqualTo(new User());
    }

    @Test
    public  void  shouldSetUsername() {
        String username = "user00";
        User builtUser = userBuilder.setUsername(username)
                .build();
        Assertions.assertThat(builtUser.getUsername())
                .isEqualTo(username);
    }

    @Test
    public  void  shouldSetPassword() {
        String password ="VeryHardPass123##";
        User builtUser = userBuilder.setPassword(password)
                .build();
        Assertions.assertThat(builtUser.getPassword())
                .isEqualTo(password);

    }

    @Test
    public  void  shouldSetRoleAsNone() {
        String emptyRole = null;
        User builtUser = userBuilder.setRole(emptyRole)
                .build();
        Assertions.assertThat(builtUser.getRole())
                .isEqualTo(RoleTypes.NONE);
    }

    @Test
    public  void  shouldSetRoleFromString() {
        RoleTypes role = RoleTypes.DESIGNER;
        String roleAsString = role.name();
        User builtUser = userBuilder.setRole(roleAsString)
                .build();
        Assertions.assertThat(builtUser.getRole())
                .isEqualTo(role);
    }
    @Test
    public  void  shouldSetRole() {
        RoleTypes role = RoleTypes.DESIGNER;

        User builtUser = userBuilder.setRole(role)
                .build();
        Assertions.assertThat(builtUser.getRole())
                .isEqualTo(role);
    }
    @Test
    public void  shouldCreateUserFromDto() {
        Long id = null;
        String username = "user007";
        String password = "HardPass17.";
        String role = RoleTypes.PRESIDENT.name();

        UserDto source = new UserDto(username,password,role);

        User effect = userBuilder.buildEntityFromDto(source);
        User expected = new User(id, username, password, RoleTypes.PRESIDENT);
        Assertions.assertThat(effect).isEqualTo(expected);
    }



}
