package pl.pp.back.patterns.builder;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.pp.back.component.notes.NoteBuilder;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.notes.Note;


public class NoteBuilderTest {

    NoteBuilder noteBuilder;

    @Before
    public void setup() {
        noteBuilder = new NoteBuilder();
    }

    @Test
    public void shouldReturnEmptyNote() {
        Note fromBuilder = noteBuilder.build();
        Assertions.assertThat(fromBuilder)
                .isEqualTo(new Note());
    }

    @Test
    public void shouldSetTopic() {
        String topic = "new topic";
        Note fromBuilder = noteBuilder.setTopic(topic)
                .build();
        Assertions.assertThat(fromBuilder.getTopic())
                .isEqualTo(topic);
    }

    @Test
    public void shouldSetContent() {
        String content = "new content to my note";
        Note fromBuilder = noteBuilder.setContent(content)
                .build();
        Assertions.assertThat(fromBuilder.getContent())
                .isEqualTo(content);
    }

    @Test
    public void shouldBuildNoteFromDto() {
        Long id = null;
        String topic = "new topic";
        String content = "new content to my note";
        NoteDto noteDto = new NoteDto(topic, content);
        Note builtByDto = noteBuilder.buildEntityFromDto(noteDto);
        Assertions.assertThat(builtByDto)
                .isEqualTo(new Note(id, topic, content));
    }

}
