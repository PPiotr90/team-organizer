package pl.pp.back.patterns.builder;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import pl.pp.back.component.projects.ProjectBuilder;
import pl.pp.back.component.projects.ProjectDto;
import pl.pp.back.component.projects.Project;

import java.time.LocalDate;


public class ProjectBuilderTest {

    private ProjectBuilder projectBuilder = new ProjectBuilder();
    @Test
    public void  shouldReturnEmptyProject() {
        Assertions.assertThat(projectBuilder.build())
                .isEqualTo(new Project());
    }
    @Test
    public  void  shoulSetLeader() {
        String leader = "leader";
        Project builtProject = projectBuilder.setLeader(leader)
                .build();
        Assertions.assertThat(builtProject.getLeader()).isEqualTo(leader);
    }
    @Test
    public  void  shouldSetDescription() {
        String description = "this is my favourite project";
        Project builtProject = projectBuilder.setDescription(description)
                .build();
        Assertions.assertThat(builtProject.getDescription())
                .isEqualTo(description);

    }
    @Test
    public  void shouldSetTopic() {
        String topic = "topic007";
        Project builtProject = projectBuilder.setTopic(topic)
                .build();
        Assertions.assertThat(builtProject.getTopic())
                .isEqualTo(topic);
    }
    @Test
    public  void shouldSetFinishDate() {
        LocalDate finishDate = LocalDate.of(2021,10,10);
        Project builtProject = projectBuilder.setFinishDate(finishDate)
                .build();
        Assertions.assertThat(builtProject.getFinishDate())
                .isEqualTo(finishDate);
    }
    @Test
    public  void  shouldSetNumber() {
        Project project = new Project();
        String prefix = "PP";
        project.setId(7L);
       project =  ProjectBuilder.setNumber(project, prefix);
        Assertions.assertThat(project.getNumber()).isEqualTo("PP-7");
    }
    @Test
    public  void shouldCreateProjectFromDto() {
        Long id = null;
        String leader = "leader";
        String description = "this is my favourite project";
        String topic = "topic007";
        LocalDate finishDate = LocalDate.of(2021,10,10);

        ProjectDto projectDto = new ProjectDto(topic,"PP", finishDate, leader, description);

        Project projectFromDto = projectBuilder.buildEntityFromDto(projectDto);
        Project expected = new Project(id, topic,null, finishDate,description, leader );
        Assertions.assertThat(projectFromDto).isEqualTo(expected);
    }

}
