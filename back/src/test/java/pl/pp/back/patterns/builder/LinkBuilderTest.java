package pl.pp.back.patterns.builder;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.pp.back.component.links.LinkBuilder;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.links.Link;



public class LinkBuilderTest {

    LinkBuilder linkBuilder;
    @Before
    public  void  setup() {
        linkBuilder = new LinkBuilder();
    }

    @Test
    public  void  shouldReturnNewLink() {
        Link built = linkBuilder.build();
        Assertions.assertThat(built)
                .isEqualTo(new Link());

    }

    @Test
    public  void  shouldSetName() {
        String name = "link1";

        Link built = linkBuilder.setName(name)
                .build();
        Assertions.assertThat(built.getName())
                .isEqualTo(name);

    }

    @Test
    public  void  shouldSetContentWithHttp() {
        String content = "www.point.pl";

        Link built = linkBuilder.setContent(content)
                .build();
        Assertions.assertThat(built.getContent())
                .isNotEqualTo(content)
                .startsWith("http://");
    }
    @Test
    public  void  shouldSetContentWithoutAddHttp() {
        String content = "http://www.point.pl";

        Link built = linkBuilder.setContent(content)
                .build();
        Assertions.assertThat(built.getContent())
                .isEqualTo(content);
    }
    @Test
    public  void shouldCreateLinkFromDto() {
        String name = "link1";
        String content = "http://www.point.pl";
        Long id = null;
        LinkDto linkDto = new LinkDto(name, content);
        Link built = linkBuilder.buildEntityFromDto(linkDto);

        Assertions.assertThat(built)
                .isEqualTo(new Link(id, name, content));
    }

}
