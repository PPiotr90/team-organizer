package pl.pp.back.patterns;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import pl.pp.back.component.commons.AbstractEntity;
import pl.pp.back.component.commons.EntityFactory;
import pl.pp.back.component.links.Link;
import pl.pp.back.component.links.LinkDto;
import pl.pp.back.component.messages.Message;
import pl.pp.back.component.messages.MessageDto;
import pl.pp.back.component.notes.Note;
import pl.pp.back.component.notes.NoteDto;
import pl.pp.back.component.projects.Project;
import pl.pp.back.component.projects.ProjectDto;
import pl.pp.back.component.users.User;
import pl.pp.back.component.users.UserDto;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class EntityFactoryTest {

    private EntityFactory entityFactory;

    @Before
    public  void  setup() {
        entityFactory  = new EntityFactory();

    }
    @Test
    public  void  shouldReturnUser() {
        UserDto userDto = new UserDto("username", "pass", "DIRECTOR");

       AbstractEntity result =  entityFactory.createEntity(userDto);

        Assertions.assertThat(result).isExactlyInstanceOf(User.class);
    }
    @Test
    public  void  shouldReturnProject() {
        String leader = "leader";
        String description = "this is my favourite project";
        String topic = "topic007";
        LocalDate finishDate = LocalDate.of(2021,10,10);

        ProjectDto projectDto = new ProjectDto(topic,"PP", finishDate, leader, description);
        AbstractEntity result = entityFactory.createEntity(projectDto);
        Assertions.assertThat(result).isExactlyInstanceOf(Project.class);
    }

    @Test
    public  void  shouldReturnNote() {
        String topic = "new topic";
        String content = "new content to my note";
        NoteDto noteDto = new NoteDto(topic, content);
        AbstractEntity result = entityFactory.createEntity(noteDto);
        Assertions.assertThat(result).isExactlyInstanceOf(Note.class);
    }
    @Test
    public  void shouldCreateLink() {
        String name = "link1";
        String content = "http://www.point.pl";
        Long id = null;
        LinkDto linkDto = new LinkDto(name, content);
        AbstractEntity result = entityFactory.createEntity(linkDto);
        Assertions.assertThat(result)
                .isNotNull().isExactlyInstanceOf(Link.class);
    }
    @Test
    public  void  shouldReturnMessage() {
        String topic = "topic";
        String content = "content";
        String sender = "sender";
        String receiver = "receiver";
        LocalDateTime send = LocalDateTime.now();
        MessageDto messageDto = new MessageDto(topic,content, sender, receiver);
        AbstractEntity result = entityFactory.createEntity(messageDto);
        Assertions.assertThat(result)
                .isNotNull().isExactlyInstanceOf(Message.class);

    }

}
